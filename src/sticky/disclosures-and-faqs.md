# Disclosures and FAQ

This page is created due to a desperate need to document some stuff I
implemented on the site. At first I crammed them in the [about](about.html) page,
but found out that the details are getting too long and boring, that it deserves
a separate page. So here they are.

---
### Website Technology

**The Blogging System**\
Blogging is part of the fun, and creating and modifying a reading experience is
another. I've tried out [Hugo](https://gohugo.io/) and [Hexo](https://hexo.io/), and they are honestly pretty
great and flexible. But since I simply had to have more control over the
styling, I eventually retracted to writing my own pages in HTML. The current
goal is to write some shell scripts to prevent myself from writing raw HTML again.
(If you haven't written pure HTML with CSS, do give it a try. It's fun and horrible
at the same time.)

Currently my update script does the following:

- Update the content with [Pandoc](https://pandoc.org)
- Add outer HTML (headers and footers)
- Create tag pages (like the [Blerbs](/tags/blerbs.html) page)
- Create the tag collection page and the archive page

The RSS and sitemap generation are now embedded in the main script. They are text files anyway, so a bit of text wrangling would suffice :)

**Why Codeberg?**\
This one boils down to personal preference. I had put my pages on
[Github](https://github.com), but did not feel comfortable with it anymore
(megacorp stuff). In reality anywhere outside of megacorps would be fine, but I
chose Codeberg because of its similarities to the Github Pages workflow. In a
parallel world I'd probably choose [Sourcehut Pages](https://srht.site/).

(The Github counterpart now lives on as [notes.ltlnx.tw](https://notes.ltlnx.tw), but I haven't decided what to do with it yet.)

**Typography and Fonts**\
I used to have some very specific typography choices, but for the new look I decided to rely on the browser to style most things, and only intervene with CSS whenver I disagree with the defaults. Turns out browsers do a pretty good job styling HTML documents, and I only need to set a maximum width, tweak some colors and margin/padding settings.

The default font is set to Noto Serif, New York (Apple devices) and Georgia, falling back in this order.

To set up default `sans-serif`, `serif` and `monospace` fonts:

- Here is the Chrome settings link: [chrome://settings/fonts](chrome://settings/fonts).
- For Firefox, go to [about:preferences#general](about:preferences#general) ￫
  "Fonts" ￫ "Advanced" to set up fonts for different languages.
- For other browsers, please refer to your browser's documentation.

**Comments — Why no comments section?**\
I haven't found a satisfactory way to implement comments without adding
hundreds of kilobytes to the load. Currently an average page _plus_ CSS weighs
about 4–5 kilobytes, and I'm not ready to let the extra bytes seep in, so
currently there's no comment section. However, I'm looking toward adding it to
every post as a separate page, or maybe implement a guestbook page. So it's 
still possible that there will be one in the future.

### Disclosures
These are the things that has possibility of shaping my opinions.

**Taiwan**\
I grew up in **Taiwan**, and my world view and common sense are heavily influenced
by this island. 

**Views on Software**\
I root for **FOSS software**, even if they are slightly inferior to their proprietary 
counterparts; I use the GIMP and Inkscape for creating graphics, LibreOffice and
Pandoc for document preparation, hardened Firefox as the browser, and Kdenlive for
video editing. (The hardest part is video editing software, as most FOSS solutions
of this regard are either too unstable or too simple. One of the most featureful FOSS
editors, Kdenlive, renders _very_ slow and crashes often for example.) _(2023-11-14 update: it may not be the case now, I've yet to catch up with developments this year.)_

**Affiliates & Advertising**\
Currently there is no affiliates and advertising whatsoever. If I did accept some
kind of affiliation I'll update it here.

(_That's it for now. I'd update the list if necessary_)

_最後更新：
2023-11-14_

標籤：其他
