# Linux One-Line Solutions

This is a collection of one-line (or maybe under-ten-lines) solutions I accumulated from the Web or other places.

_最後更新：
2023-04-24_

---

### Wrangling text with sed, perl and friends

**Capitalize every word**

```
sed 's|\b\(.\)|\u\1|g'
```

More sed one-liners at <https://edoras.sdsu.edu/doc/sed-oneliners.html>

### FFMPEG 

**Transcoding into webm**

```
ffmpeg -i input.mp4 -c:v libvpx-vp9 -crf 30 -b:v 0 -b:a 128k -c:a libopus output.webm
```

**Adding a black video to audio (for posting, etc.)**

```
ffmpeg -f lavfi -i color=size=1280x1280:rate=60:color=black -i input.mp4 -c:a mp3 -shortest output.mp4
```

標籤：其他
