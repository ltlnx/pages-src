# 首頁

歡迎來到我的網站！所有貼文都在[庫存](/archive.html)，貼文標籤有專門的[標籤頁](/tags)，而[關於](about.html)頁面放了一些個人資訊。

這個網站也有 [RSS 消息來源](/rssfeed.xml) 可供訂閱。RSS
就是網路上拿來「追蹤」人的古老方法之一。

如果您有任何事情想與我分享，或是想對網站提供建議，請用電子郵件或其他管道聯絡我。聯絡資訊[在這裡](https://ltlnx.tw/about.html#聯絡資訊)。祝您玩得愉快！

值得一提：

- 我的[網誌產生器](https://ltlnx.tw/blog-generator.html)，用來將文字檔轉換為這個網誌。
- [Inkscape](https://inkscape.org/)，我貢獻過翻譯的向量繪圖軟體。
- 我對 Slackware
  的[第一印象](https://ltlnx.tw/slackware-impressions.html)。Slackware
  是我目前使用的 Linux 發行版。還有其他 Slackware 相關文章在
  [slackware](https://ltlnx.tw/tags/slackware.html) 標籤下。

> 最近在瀏覽了一些前輩的網站之後，還是決定將這個網站定位為全中文網站，因為與
> Linux
> 桌面環境、發行版設定相關的繁體中文資訊還是相對地少，而且我可以在其中練習撰寫中文文章。
> ------ltlnx 2024-01-25

------ ltlnx 2024-02-16
