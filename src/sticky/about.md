# 關於

## 我

嗨，我是高文偉，網路常用暱稱「ltlnx」與「litelinux」。

> 要知道我目前的去向，歡迎瀏覽[今朝](now.html)頁面。

興趣有：

**電腦相關**

- 測試不同的 Linux 發行版、軟體、主題、視窗管理員......
- Android 越獄（Root）、安裝自訂映像
- 撰寫簡易的網頁與用來將工作的指令稿
- 設定自架伺服器環境
- 使用不同的程式語言編寫簡易的程式

**排版與字型**

- 製作中英標準字
- （緩慢地）製作字體

**平面設計**

- 研究不同風格的設計法則（運用在學校社團、系上活動的主視覺設計）
- 指定使用自由開源軟體（GIMP、Inkscape、Krita......）
- 目前是 [Inkscape](https://inkscape.org) 譯者

**音樂相關**

- 聽音樂，什麼都聽
- 打鼓、彈一點吉他貝斯合成器
- 學習現場混音與錄音室混音

我的母語是中文（台灣），但可以用繁/簡中文或英文與我溝通，基本上還能理解。我的西班牙文還不太行，但也可以試試看用西班牙文跟我聊天 ：）

## 本站

我喜歡把個人網站弄得簡單點，所以本站沒什麼花俏的成分。目前使用的色系取材自 [Jonas Hietala's blog](https://www.jonashietala.se/)，並使用最少的 CSS 做出樣式。（如果在電腦、手機上使用暗色模式，在這裡看到的顏色是基於 Krita Dark Orange 色系。）同時我常在更樸素的瀏覽器中檢查我的網站，像是 [Netsurf](https://www.netsurf-browser.org/)、[Dillo](https://github.com/crossbowerbt/dillo-plus) 與 [Lynx](https://lynx.invisible-island.net/)，讓本站內容有更高的機率在所有瀏覽器中都能正常顯示。

這個網站處於永恆施工狀態，如果對本站的顯示方式或內容有評論、回饋或建議，歡迎直接告訴我。

## 常見問題與事實披露

有些你可能想問我、關於我或是這個網誌的常見問題，或是一些我覺得有義務要揭露、可能影響我寫作的要素，可以在[事實披露與常見問題](disclosures-and-faqs.html)文中找到。

## 關於隱私

這個網站**不會**蒐集關於您的詳細資料，但每頁下方有連結，點了可以讓我知道您瀏覽過該頁面。詳見[觀看次數的運作原理](/viewcount.html)。

## 著作權

**內容**

這個網站的內容使用[創用授權 CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)授權。同時，如果要轉載網誌文章的部分或全部，請事先[聯絡我](#聯絡資訊)。

歡迎您備份我的網站在自己的磁碟上，但請不要通篇複製到內容農場上。
如果要備份本站，請至 [Codeberg 原始碼頁面](https://codeberg.org/ltlnx/pages) 並點擊右上角的下載 (Download) 按鈕。您也可以使用 git clone：
```
git clone https://codeberg.org/ltlnx/pages.git
```

**程式碼**

所有用來（重新）建置本網站的程式碼皆使用 MIT 授權。目前的網站建置指令稿在[這裡](https://github.com/ltlnx/ltlnx-blog-updater)（警告：Github 連結），網站程式碼在[這裡](https://codeberg.org/ltlnx/pages-src)。

## 網站更新

技術層面的網站更新見[網站更新紀事](website-log.html)。

## 聯絡資訊

- Codeberg：[@ltlnx](https://codeberg.org/ltlnx/)
- 聯邦宇宙：[@ltlnx@g0v.social](https://g0v.social/@ltlnx)
- 電子郵件：ltlnx a disroot d com (d=dot, a=at)
- Telegram：+886907824182
- Matrix：@ltlnx:matrix.org

（使用 Telegram 與 Matrix 請先用電子郵件告知，以便互加好友）

最後更新於
2024-04-16

標籤：其他
