# 連結

這裡放的連結，有我常閱覽的網站、好用的資訊，與這幾年在網路上搜羅來的連結。隨著清單越來越長，建議您使用目錄或是瀏覽器中的「在頁面中搜尋」功能尋找需要的資訊。

如果您碰到任何失效的連結，請[聯絡我](about.html#聯絡資訊).

（非英文的網站會使用該語言的代碼表示。）

---

### 分類

- [站內](#站內)
- [HTML and Web standards](#html-and-web-standards)
- [Website Testing](#website-testing)
- [Linux and System Management](#linux-and-system-management)
- [Slackware](#slackware)
- [Bash and Shell Scripting in General](#bash-and-shell-scripting-in-general)
- [News and Aggregation Sites](#news-and-aggregation-sites)
- [Audio Production](#audio-production)
- [Fonts and Typeface Designs](#fonts-and-typeface-designs)

---

### 站內

[關於](about.html)\
所有我選擇放棄的隱私。

[部落滾](blogroll.html)\
紀錄我有訂閱或是定期回訪的網誌。

[網站更新紀錄](website-log.html)\
如題。包含網站的公告、筆記與偶爾的抱怨。

[訂閱](rss.html)\
如果想接收新貼文通知，可以將此連結加入至您喜愛的 RSS 閱讀器中。（[直接訂閱連結](rssfeed.xml)）

<small>[回到分類總表](#分類)</small>

---

### HTML 與網際網路標準

[The HTML Standard](https://html.spec.whatwg.org/)\
官方的 HTML 規格表。
The official specsheet of HTML. For creating HTML rendering tools or
better semantic HTML. Do note that this is a _"Living Standard"_, which means
that it may be changed any time. (Not a fan of this but well...)

[Introduction to Atom](https://validator.w3.org/feed/docs/atom.html)\
The format I currently use for syndication. I _feel_ like there's no advantage for
me to choose it over [RSS 2.0](https://cyber.harvard.edu/rss/rss.html), but it's a fine format nonetheless
that tries to present more info to feed readers.

[W3 Requirements for Chinese Text Layout](https://www.w3.org/TR/clreq/)\
A good reference for typesetting Chinese text on the Web and other places. 
Includes basics on typefaces, page design, writing modes, line composition
(where to break lines), punctuation marks, etc. Though not all of the requirements have
been regulated in the HTML spec, it gives us a high-level understanding of how
Chinese text _should_ look like on the Web.

[W3 Requirements for Japanese Text Layout](https://www.w3.org/TR/jlreq/)\
A similar document to the above, but for Japanese text. Japanese shares a lot of
characteristics with Chinese, therefore this also serves as a resource for
typesetting Chinese. Plus, this is commisioned far earlier than the Chinese
version and is therefore more stable and has more implemented standards.

<small>[回到分類總表](#分類)</small>

---

### Website Testing

[SSL Server Test](https://www.ssllabs.com/ssltest/index.html) \
Tests which cipher suites a web server has.

[WebPageTest](https://www.webpagetest.org/)\
Test various metrics of websites, like load speed, contentful paints, etc., 
and offers waterfall views of loads. Useful to test websites in various
locations and settings. A few months ago I wouldn't recommend it, but I've
tried it again and it seems okay.

[GTMetrix](https://gtmetrix.com/)\
Another webpage test site I encountered a while ago. It has almost identical
offerings with WebPageTest, but starting from 2023-12-01 it would require a
login to do basic tests. I'm not blaming them as it's a quick way to defer
spam and fraud, but I don't thing not allowing guest tests would do them
good in the long run. We'll see.

<small>[回到分類總表](#分類)</small>

---

### Linux and System Management

[DistroWatch](https://distrowatch.com/) \
DistroWatch is a website dedicated to Linux, BSD and OpenSolaris distributions,
featuring distro introduction pages, bundled software version overviews,
comments on distros, distro popularity rankings, and more. On the homepage,
they also have distro update briefings and one of my favorite reads on the web,
DistroWatch Weekly. If you're a  Linux enthusiast and distro-hopper like me,
you would probably love this site.

[鳥哥的Linux私房菜](https://linux.vbird.org/) (zh-TW)\
An inclusive resource for learning Linux and system administration in general.
Covers mostly RHEL-based OSes (RHEL, CentOS, Rocky/Alma) and discusses a broad
range of Linux-related topics in detail. Definitely a great place to go if you
speak/read Chinese and are interested in sharpening your Linux skills.

[PTT 看板 Linux](https://www.ptt.cc/bbs/Linux/index.html) (zh-TW)\
Discussion on Linux in general, in Traditional Chinese. Requires a PTT account.

<small>[回到分類總表](#分類)</small>

---

### Slackware

Since I use Slackware on a daily basis, I've also accumulated a number of Slackware-relevant links. They are available on a separate page under the Slackware category. Please follow this [link](/slacklinks.html) to the page.

<small>[回到分類總表](#分類)</small>

---

### Bash and Shell Scripting in General

[Alien's Bash Tutorial](https://subsignal.org/doc/AliensBashTutorial.html)\
An exceptionally good tutorial on UNIX/Linux structure, common commands, bash
and bash scripting.

[Handy one-liners for SED](https://edoras.sdsu.edu/doc/sed-oneliners.html)\
More that often we need to mangle with text, and one of the more powerful tools
to do that is `sed`. This page contains some useful knowledge when approaching
it, as well as a large number of one-liners ready to apply into your scripts.

<small>[回到分類總表](#分類)</small>

---

### News and Aggregation Sites

[Hacker News](https://news.ycombinator.com)\
A news aggregation site based on user submissions, with an upvote system to
ensure quality. There is a comments page for every submission, and in the
comments, often there are high-quality discussions from people who know the
subject well. As the name suggests, submissions usually surround tech,
engineering and things that interest these kinds of people.

[The Register](https://www.theregister.com)\
A technology news site from the UK. Reports on systems administration, Linux
distributions, tech regulation changes, software, coding tools and languages,
etc. What I like a lot is their tech columns, which features sysadmins who
fucked up databases, cringey management or bosses, tech support woes, and the
like — and besides being entertaining, it gives us a glimpse into the real-life
tech world.

<small>[回到分類總表](#分類)</small>

---

### Audio Production

[Recording Hacks](http://recordinghacks.com/)\
A site about microphones, speakers and ways to use them. The site looks like a
2008-esque Wordpress site, which is an immediate indication of quality.

[FreeStompBoxes](https://www.freestompboxes.org/)\
A forum site discussing about effect pedals for guitars and bass guitars.

[LinuxMusicians](https://linuxmusicians.com/)\
A forum for — you guessed it — musicians and producers on Linux.

[Linuxaudio.org Resources](https://linuxaudio.org/resources.html)\
A site (and related sites) dedicated to Linux audio production. They also maintain
the [Libre Music Production](https://linuxaudio.github.io/libremusicproduction/html/) static archives.

<small>[回到分類總表](#分類)</small>

---

### Fonts and Typeface Designs

[貓啃網](https://www.maoken.com/)\
A collection of free and paid Chinese fonts. Probably contains the world's largest
Chinese free font database.

[Font Library](https://fontlibrary.org/)\
Another collection of fonts with various licenses. Again, the 2008-esque web design
is a sign of quality.

[Google Fonts](https://fonts.google.com/)\
I hate to link to anything Google, but Google Fonts is a good source of free fonts
that permits commercial use. Just make sure you always download the fonts, and not
link to them directly, or you may be [violating the GDPR](https://www.theregister.com/2022/01/31/website_fine_google_fonts_gdpr/).

[Typewolf Typography Resources](https://www.typewolf.com/resources)\
A page containing links to some other type resources.

[Justfont Blog](https://blog.justfont.com/)\
The blog of Taiwanese font foundry Justfont. Though their recent posts are mostly
self-promotion, their old posts contains some valuable insights on CHinese typeface
design.

<small>[回到分類總表](#分類)</small>

---

That's all ¯\\\_(ツ)\_/¯

2024-11-20

標籤：其他
