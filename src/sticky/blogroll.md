# 部落滾

「部落滾」這個新詞是從 [O3noBlog](https://blog.othree.net/blogroll/) 借來的，相對於英文的 blogroll。在這裡我會列出平常有訂閱或是會定期閱覽的網站或網誌。

最後更新於
2024-08-06

---

## 網路夶們的網誌
- [O3noBLOG](https://blog.othree.net/) \
  網頁前端開發者，什麼主題都有。
- [Jedi's BLOG](https://jedi.org/blog/) \
  與網頁親和力、助聽器與廣泛科技相關的部落格。
- [gugod's blog](https://gugod.org/) \
  Perl、Linux 使用者。
- [Dev.Poga](https://devpoga.org/) \
  宇宙小酒館 [g0v.social](https://g0v.social) 維護者愛貓波嘎氏的網誌。
- [hiroshi yui](https://ghostsinthelab.org/) \
  開源軟體社群 OG 之一，[樸實注音鍵盤](https://github.com/hiroshiyui/GuilelessBopomofo)作者。
- [Gea-Suan Lin's BLOG](https://blog.gslin.org/) \
  常常轉貼各種科技新知，並加以評論。
- [Ivon的部落格](https://ivonblog.com/) \
  文章主要與手機刷機、Linux、開源軟體相關。
- [Freedom Wolf](https://www.freedomwolf.cc/) \
  g0v.social 常客，Linux 相關技術文章為主。
- [FizzyElt](https://fizzyelt.com) \
  也是 g0v.social 常客，前端文章與 Functional Programming 相關文章為主。
- [ᕕ( ᐛ )ᕗ Herman's blog](https://herman.bearblog.dev/) \
  Bear Blog 主要維護者，生活取向，但也有零星的技術文章。

## 報章雜誌
- [Distrowatch Weekly](https://distrowatch.com/weekly.php) \
  Distrowatch 每週發行的雜誌，包含 Linux 發行版評測、科技相關新聞與觀眾問答。
- [LWN](https://lwn.net/) \
  Linux kernel 相關的所有新聞，有時也有發行版相關新聞。


---

標籤：其他
