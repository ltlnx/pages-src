# 訂閱

如果您是與我同齡或比我小的人，可能不知道在網路上，除了能訂閱影片頻道與網路服務（或是訂閱報紙）之外，還可以訂閱部落格文章。訂閱文章的技術稱為 RSS，是由網站提供一個檔案連結供閱讀軟體「訂閱」，裡面包含文章標題、作者，與選配的圖片、文章內容。

常見的閱讀軟體有：

- 網站 [Inoreader](https://www.inoreader.com/)
- 瀏覽器擴充功能 [FeedBro](https://nodetics.com/feedbro/)
- macOS/iOS/iPadOS 上的 [NetNewsWire](https://netnewswire.com/) （[使用教學](https://free.com.tw/netnewswire/)）
- Android 上的 [Feeder](https://github.com/spacecowboy/Feeder)

---

如果要訂閱這個網誌，請用您最愛的 RSS 閱讀器開啟此連結：

- [訂閱主要 RSS 訊息來源](rssfeed.xml)

如果有問題、評論或建議，請[聯絡我](about.html#聯絡資訊).

最後更新：
2024-02-23

標籤：其他
