# 網站更新紀錄

這裡有網站相關的更新說明、筆記與偶爾的抱怨 —— 大多是後台發生的事情。他們通常與我的[提交訊息](https://codeberg.org/ltlnx/pages/commits/branch/main)相關。

_最後更新於
2024-03-21_

------------------------------------------------------------------------

*2024-03-21*

> ## TL;DR
> 
> - 更新一些標籤的措辭

「公開紀錄」成了「生活雜記」、「脫稿詞」成了「隨想」。如此一來用詞比較接近日常，沒那麼西化。當然這代表一些舊連結又要爆了，如果有人連結到這幾頁容我深深致歉。

------------------------------------------------------------------------

*2024-02-19*

> ## TL;DR
>
> - 新增暗色模式顏色

新增了基於 Krita dark orange 色系的暗色主題。

------------------------------------------------------------------------

*2024-02-16*

> ## TL;DR
>
> - 新增小螢幕上的顯示方式（標題稍微縮小）
> - 移除可替換樣式表，讓網站更輕量

------------------------------------------------------------------------

*2024-01-29*

> ## TL;DR
> 
> - 這個網站要全面中文化了！

你沒看錯，這個網站要全面中文化了！有感於繁體中文世界關於 Linux 桌面環境、應用程式等方面文章上的不足，總覺得自己要盡多點心力，寫多點關於 Linux 與開源世界相關的文章。

目前已中文化的區域有首頁、各標籤說明文字與頁首頁尾，未來會將文章慢慢翻成中文，新文章也會使用中文撰寫。至於這裡會不會變成雙語部落格呢？我目前沒這個打算，但應該會將原本的英文文章們用某種形式留存。

------------------------------------------------------------------------

*2023-12-07*

> ## TL;DR
>
> - The header hirearchy are now fixed to accomodate accessibility
>   concerns
> - Sitewide linking criteria update: only link to my pages if you're a
>   personal site or if you're sharing info on a ActivityPub-compatible
>   social media account

The headers now have the "correct" hirearchy according to the [Web
Accessibility
Initiative](https://www.w3.org/WAI/tutorials/page-structure/headings/).
I'm using `<h3>`s after `<h1>`s the whole time I've been blogging, since
it automatically gives me the right font size I want, but I guess
adopting a canonical hirearchy while fixing the font size with CSS
benefits more people. That said, I wish browsers adopt an easy way for users to
define font sizes, etc. for unstyled sites, and have a way to enforce
them (i.e. not respect the remote CSS of the page).

Another update is the linking criteria: I came across an article named
["Browsing the Eastern Side of the Personal
Web"](https://www.bikobatanari.art/posts/2023/east-west-website-culture)
today, and there is this section about [linking
criteria](https://www.bikobatanari.art/posts/2023/east-west-website-culture#linking-criteria)
that talks about having a disclaimer telling viewers to only link to
their site *only if* their usage meets a certain criteria, such as:

> The site is link-free only to creative personal sites. No need to
> worry about reporting!

This is a lovely idea that I've never thought of, so I'm adding my own
variation to the footer. I hope that people only link here on their own
personal site, or when they share my posts on federated social media
such as ActivityPub-compatible instances, and outside of this, to
contact me before they link here. So below is my take (subject to
change):

> Please only link to the site on your personal site, of when sharing on
> [ActivityPub](https://activitypub.rocks/)-compatible social media
> sites. When in doubt (or if you want to link from elsewhere), please
> [contact me](/about.html#聯絡資訊).

------------------------------------------------------------------------

*2023-11-28*

> ## TL;DR
>
> - Internal links and all resources should be reinstated

I checked with some good old `find | grep` commands to find all the
internal links and missing resources, and fixed them by hand, so they
sould (hopefully) all be working now.

------------------------------------------------------------------------

*2023-11-27*

> ## TL;DR
>
> - Internal links should be all broken by now, fix incoming

If you're a subscriber of my RSS feed, please re-subscribe --- the
breakage of links sould cause duplicates to show up in your RSS reader,
and the fastest way to solve this is to re-add the feed.

Also all internal links are now broken, and I'm on my way to traverse
them all and fix them one by one. Self-inflicted webadmin stuff.

I'm sorry. Won't change the link schema ever again.

------------------------------------------------------------------------

*2023-11-13*

> ## TL;DR
>
> - Revamped site again to properly support tags
> - Old post links are broken and I haven't found a way out

A heads-up for everyone reading this blog: the site had just been
revamped to be even more minimal, and for proper tag support. What that
means, is that the old post links would be broken --- and I'm still
thinking what to do for this, since there's no server for static sites
to redirect readers to the new locations. If you have any good idea,
please contact me (info in the about page). Sorry for the inconvenience.

Also, all the pictures are broken as something happened when I was in
the process of moving parts of the blog to new locations. I'll fix them
alond with some faulty Markdown when I have time.

------------------------------------------------------------------------

*2023-07-10*

> ## TL;DR
>
> - Style overhaul... again

Guess what, after 3 months of rocking the last CSS file, I got bored
again and changed it to look more modern and miimalistic. And make use
of a CSS hack to get responsive hamburger menus.

------------------------------------------------------------------------

*2023-03-12*

> ## TL;DR
>
> - Large style overhaul

Once again, I altered the style of this blog, this time taking
inspiration from the slackware.com site and various GNU documentation
sites.

------------------------------------------------------------------------

*2023-01-07*

> ## TL;DR
>
> - Better Chinese-English padding

A delve into the depths of Bilingual layouts led me to rewrite the
`<span>` padding generation code. Now it should look a lot more graceful
than before for Chinese readers.

------------------------------------------------------------------------

*2022-12-14*

> ## TL;DR
>
> - Added Chinese translations of category names

After this update, the Chinese translations of the categories will show
up beside their English counterparts. I am still thinking how to make
Chinese and English coexist peacefully on the site (as I write in both),
and hopefully this is a first step to a bi-lingual website.

In the future I may add ways to show translations of the same article,
or even separate the site into a Chinese version and English version.
Hard to say what would happen exactly.

------------------------------------------------------------------------

*2022-12-08*

> ## TL;DR
>
> - Added date to all article listings

I've finally added some lines to my update script to grab the last
edited (updated) date and show them beside the article link. Also, the
articles are now sorted based on it, instead of the last modified time
as defined by the computer. Enjoy!

------------------------------------------------------------------------

*2022-10-18*

> ## TL;DR
>
> - The website now has a TLD at <https://ltlnx.tw>
> - Officially removed favicons

I finally bought the TLD [ltlnx.tw](https://ltlnx.tw)! Now all content
is served over this URL.

Favicons are officially removed. Before this, browsers make favicon
requests to my server, which I don't have, and complains with error code
404. Courtesy to [this thread on
StackOverflow](https://stackoverflow.com/questions/1321878/how-to-prevent-favicon-ico-requests),
I added a line to every HTML file to work around this:

    <link rel="icon" href="data:,">

This should prevent 404's from happening.

------------------------------------------------------------------------

*2022-10-12*

> ## TL;DR
>
> - Style overhaul

Another long-awaited style overhaul. I increased the main font size and
line height on the desktop, and added styles for headings.

------------------------------------------------------------------------

*2022-09-25*

> ## TL;DR
>
> - Added trivial styles for warnings

::: {.warning}
This is the new warning style that will show on paragraphs marked with
the class `warning`.
:::

> \[Update\] This has since been removed.

------------------------------------------------------------------------

*2022-08-04*

> ## TL;DR
>
> - Fix page title generation
> - Changed sidebar layout on the desktop

A previous bug in the update code makes every page lack \<title\> tags.
(No wonder only one page was indexed by Google, and caches are not
refreshed by DuckDuckGo!) This is now fixed, and every page has titles.

The desktop sidebar has always been an annoyance to me, as it shifts the
content to the right with no benefit in looks or navigation. Now the
sidebar on the desktop is moved up to the top right; I'm still not very
satisfied by the result, but IMO it is a small improvement from the
previous layout. What do you think? Please mail your opinions to the
mailbox in my [Contact info](about.html#聯絡資訊). All opinions
appreciated :)

------------------------------------------------------------------------

*2022-07-22*

> ## TL;DR
>
> - Every directory has a page
> - Better subfolder index handling

I refactored the website update code, so that index page generation is
in a loop: for every folder, generate a page index. Making this part of
the code modular has a side benefit: I don't need separate code to
generate the homepage anymore.

Another part of code updated is, every index page now shows the first 10
entries in every subfolder, and has a "More" or "Dedicated Page" link
directing to the subfolder index.

------------------------------------------------------------------------

*2022-06-28*

> ## TL;DR
>
> - The posts now are sorted in chronological order

I didn't specify the order posts appear on the homepage, so they are
sorted by whatever default `find` outputs its results. For some reason I
decided to address this issue today, so I searched and found [this
answer](https://superuser.com/a/1413494). I modified it to output one
result per line, so the final code is the following:

    find . -type f <other arguments here> -exec ls -t1 {} +

Currently there's no dates beside the post list however; I may address
it some day...

------------------------------------------------------------------------

*2022-06-20*

> ## TL;DR
>
> - The update script is updated to use functions more
> - New footer
> - The [blerbs](../blerbs/index.html) page now has an `index.html` for
>   cleaner navigation

The updater script sees an overhaul that makes part of the code more
modular. For example, the "simple subpage layout" generation code is now
a function, so that we can generate multiple subpages with the same
code. This future-proofs possible updates to move tutorials,
documentation, etc. into subfolders.

There's a new footer to state copyright: Everyone are welcome to copy
stuff from my site or archive them as a whole, but they should release
the content in compliance of the [Creative Commons BY-SA
4.0](https://creativecommons.org/licenses/by-sa/4.0/) license. Other
terms of copying are on the [About](about.html) page.

Another thing to cover is that subfolder pages now have their own
`index.html` page, instead of the `all-<subfolder name>.html` format.
This makes navigation cleaner, as we only need to key in
<https://ltlnx.codeberg.page/blerbs/> instead of
<https://ltlnx.codeberg.page/blerbs/all-blerbs.html>.

------------------------------------------------------------------------

*2022-05-29*

> ## TL;DR
>
> - The dark theme is now proudly powered by the [Nord color
>   palette](https://www.nordtheme.com/docs/colors-and-palettes)! Shout
>   out to the great guys at Nord.

The dark theme is now powered by the all-hailed [Nord
colorscheme](https://www.nordtheme.com/)! Since the more I look at the
original dark palette, the more it looks like it's drowned in molasses
(or whisky if you're in the mood), I went in search for a cleaner
palette. After trying some other color palettes, I found Nord, highly
praised by critics as a serene dark theme with suitable contrast. I
tried it on Vim for a week and loved it, so I thought it's time to apply
it on the website.

However, I also chose some undocumented colors that Nord uses on their
website, including:

- [██]{style="color:#242933;letter-spacing:-0.1em"} `#242933`, a shade
  darker than the [██]{style="color:#2e3440;letter-spacing:-0.1em"}
  "nord0" color, for the background;
- [██]{style="color:#abb9cf;letter-spacing:-0.1em"} `#ABB9CF`, two
  shades lighter (I reckon) than the
  [██]{style="color:#4c566a;letter-spacing:-0.1em"} "nord3" color, for
  quote text;
- [██]{style="color:#638897;letter-spacing:-0.1em"} `#638897`, one shade
  darker than the [██]{style="color:#88c0d0;letter-spacing:-0.1em"}
  "nord8" color, for quote left borders.

Overall, this change gives us a cleaner dark theme experience. The
[about](about.html) page is updated as well to reflect the change.

------------------------------------------------------------------------

*2022-05-26*

> ## TL;DR
>
> - Reverted font choice for Android devices back to Roboto to address
>   legibility issues
> - Added a dedicated RSS page
> - Modified the title-grabbing technique in my update script; it now
>   grabs the `<h1>` title instead of parsing the filename

This time there is no detailed explanation, as life keeps getting in the
way these days.

------------------------------------------------------------------------

*2022-05-23*

> ## TL;DR
>
> - New "source-sans-pro" style rule targetting Android
> - Subscribe to RSS from the URL bar in supported browsers

I finally found a way to target Android fonts without it defaulting to
Roboto. Turns out we shouldn't target the font name like in other OSes,
instead target the generic name defined by `/etc/fonts.xml` on Android.
Fortunately, there's a copy of `fonts.xml` [available
online](https://android.googlesource.com/platform/frameworks/base/+/refs/tags/android-cts-11.0_r8/data/fonts/fonts.xml),
so we can use it as a reference. In the end, I chose "source-sans-pro"
(which is identical to Noto Sans) as the default display and text font
on Android.

Android font support, at this point, still leaves a lot to be desired.
In the "embedded device" mindset of early Android, storage is scarce and
left for other applications, therefore most fonts are only aliases in a
"font definition" file, and there are only a handful of fonts available
on the device.

However currently, where mobile storage easily surpasses desktop ones,
Google (or the Open Handset Alliance, whatever) still didn't put better
fonts on Android devices, and only added Noto fonts for language
support. Come on, it's 2022! Why cripple a key point in good UI for
reasons that isn't a concern anymore?

Another change is the ability to subscribe to the site by simply
clicking the RSS button in the URL bar. It once was a Firefox feature,
but Firefox [phased it
out](https://support.mozilla.org/en-US/kb/feed-reader-replacements-firefox)
in favor of their "Pocket" service. What a shame! Luckily, the feature
lives on in some Firefox derivatives like Pale Moon.

IE also supported RSS subscriptions, but sadly the browser's obsolete at
this point. Chrome didn't support such stuff from the beginning, but the
functionality can be added with an [official Google
extension](https://chrome.google.com/webstore/detail/rss-subscription-extensio/nlbjncdgjeocebhnmkbbbdekmmmcbfjd).

To make the RSS icon show, I added an RSS feed link to every `<head>`:

    <link rel="alternate" type="application/rss+xml" title="All Pages on the Website" href="https://ltlnx.codeberg.page/rssfeed.xml">

Then, *ta-da*! The rss icon shows in the address bar:

![RSS icon in the address bar](/res/rss-feed-icon-palemoon.png)

------------------------------------------------------------------------

*2022-05-19*

> ## TL;DR
>
> - New sitemap
> - New RSS feed
> - All webpages now have `<title>`s

There's a new [sitemap](https://ltlnx.codeberg.page/sitemap.xml) for the
website! Generated by a [bash
script](https://codeberg.org/ltlnx/pages-src/src/branch/main/sitemap)
based on [mcmilk's script](https://github.com/mcmilk/sitemap-generator),
hopefully it would bring a bit of SEO benefit to this blog. Also, I
implemented a RSS feed, generated by a [heavily modified
version](https://codeberg.org/ltlnx/pages-src/src/branch/main/rss-roller)
of maxhebditch's
[rss-roller](https://codeberg.org/ltlnx/pages/commits/branch/main) .
Both are bash scripts, which is easier for me to modify and maintain.

Originally there are no `<title>` attributes in all my webpages, since I
used a shared header template to generate them. Today I added some `sed`
trickery to extract titles from original markdown files, and add them as
`<title>` elements. This should also have a positive effect in SEO.

------------------------------------------------------------------------

*2022-05-18*

> ## TL;DR
>
> - New dark theme for `@media(prefers-color-scheme: dark)` guys
>   - Inspired by the vim colorscheme "Slate"
>   - But with less colors
> - `sed` expression to convert arrows to better-looking arrows
> - Added hover highlights

So I eventually put out a dark theme, and minified the css a bit. Not in
the sense of emitting all newlines, as I absolutely *despise* that; more
like sharing some style blocks and removing newlines for elements with
only one style.

The dark theme is inspired by vim's "Slate" theme, which is the only
theme in vim with a softer color palette; others are razor-sharp and
often an eyesore. However, the colorscheme features a lot of colors, and
when I tried them on different elements, I found out that I can't
harmonize them perfectly. So I retreated to one main color, and other
auxillary colors referring to it.

Currently, the text color of the dark theme is kind of a washed-out
orange, malt-like color. I'm thinking whether to make it plain white ---
just like how the original Slate theme treats text. I think I'm going to
keep it until people complain.

The website generation script also had a small change: due to my abuse
of code-style arrows (-\>), I infused a sed expression to convert it to
an arrow (￫). However, currently it converts code representations to
arrows too, but I haven't found an easy fix to it.

I might as well type a proper arrow with the help of the *compose* key
(which is as easy as `Compose + hyphen + greater-than`), but in Noto
Sans, the default arrow looks like this:

←↑↓→

Which, for some reason, looks bad in desktop Firefox. So I decided to
use Unicode codepoints FFE9 to FFEC (￩￪￬￫), which has a more consistent
look across devices.

Another change is that links now have non-intrustive hover highlights,
which used some CSS trickery to make the highlighted link stay in place.
The technique is copied from [a reply on CSS Tricks
website](https://css-tricks.com/forums/topic/fixed-width-highlight-on-hover/).
The answer was missing a `margin-right` but that's an easy fix.

標籤：其他
