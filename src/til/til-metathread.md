# Today I Learned Metathread

All things I learned that doesn't count as a post, in reverse
chronological order.

_Last updated: 2024-01-24_

## 2024-01-24

To clean up local git branches deleted on a remote upstream, Erik
Schierboom provided [a way to delete them at
once](https://www.erikschierboom.com/2020/02/17/cleaning-up-local-git-branches-deleted-on-a-remote/).
To summarize, you can issue the following commands in the terminal:

    git fetch -p && \
    git for-each-ref --format '%(refname:short) %(upstream:track)' | \
      awk '$2 == "[gone]" {print $1}' | \
      xargs -r git branch -D

The linked article above also lists another command that aliases the
process to `git gone`:

    git config --global alias.gone "! git fetch -p && git for-each-ref --format '%(refname:short) %(upstream:track)' | awk '\$2 == \"[gone]\" {print \$1}' | xargs -r git branch -D"

## 2023-11-30

Some days ago I was merging two audio tracks and combining them with a
video, and pleasantly found out that `.mov` extensions can have `.wav`
(`pcm_s16e`) as the audio track format, while `.mp4`s cannot. So I can
extract the audio, edit them in Ardour, export them into wav, and
combine with video into a mov container *without any proprietary aac
stuff involved*. Which means I don't need another version of ffmpeg, and
can use the system-provided one.

Most phones play .mov anyway, so what's to lose?

## 2023-11-28

I just came across [Michał "rysiek" Woźniak's
blog](https://rys.io/en/index.html) which has this gorgeous black-orange
style and curved lines. The placement of the menu is *chef's kiss* too,
as it just blends into the header and feels super natural. Definitely an
inspiration if I were to overhaul the visuals of this blog.

## 2023-05-29

**Linux quick fix: how to connect to a previously connected network
without waiting**

If you're using the `wl` driver on Macs like me, you may know that the
driver more than ofter has problems with establishing connections. The
reproducible solution to quickly connect to a previously connected
network while connected to another Wi-Fi is the following:

(Do note that I'm using KDE Plasma. Procedures may differ on other
setups)

1.  Press "connect" on the Wi-Fi you are to connect to.
2.  Wait for about 2 seconds (with the dialog saying "Establishing
    connection...").
3.  Disable Wi-Fi.
4.  Re-enable Wi-Fi. It may start to try connecting to another network
    (if the network is present).
5.  Press connect on the desired Wi-Fi SSID again.

Profit.

## 2023-05-28

[NotoCJK](https://github.com/simonsmh/notocjk)  
Android Magisk module patching Noto Sans & Serif CJK to support all
weights.

## 2023-05-17

[FreeStompBoxes](https://www.freestompboxes.org/)  
A Guitar / Bass pedals DIY forum.

## 2023-04-11

[Use Appimages in portable
mode](https://docs.appimage.org/user-guide/portable-mode.html)

標籤：本日新知
