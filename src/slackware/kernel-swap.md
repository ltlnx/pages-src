# Install and Boot the Linux Mint Kernel on Slackware

> ### Warning
>
> Do note that this procedure is HIGHLY discouraged. While booted into Slackware with the Linux Mint kernel, I face some issues such as
> 
> - Spotty trackpad and drawing tablet support. When the drawing tablet is plugged in, the trackpad starts to misbehave or not work at all.
>  
> Other problems that I can't recall at the time of writing also exists. Expect buggy performance, and please do not do this on production devices.

### Preface
Since I upgraded my laptop from kernel version 5.14.1 to 5.15.12, sleep (suspend) stopped working. I switched all the power actions in KDE from "Sleep" to "Hibernate" as a workaround. However recently I tested Linux Mint on this laptop, and sleep worked as expected, so I wonder if this has something to do with the kernel or kernel modules. 

To test if this is the case, I swapped the Linux Mint kernel and modules onto Slackware and booted it. Magically, sleep started working. This is a guide on how I made it work.

### Specs
- Laptop: MacBook Pro Late 2013 (MacBookPro11,2)
- CPU: Intel Core i7-4770HQ
- RAM: 16 GB
- Network card: Broadcom BCM4360
- Slackware version: -current w/ kernel 5.19.6
- Linux Mint version: 21.1 "Vanessa" w/ kernel 5.15.0-47
- Bootloader: ELILO

### Procedure
**In Slackware**

- Partition your Slackware box and create (or make sure there is) a partition larger than 20GB.
- Download the [Linux Mint ISO](https://linuxmint.com).
- Install Linux Mint on the partition you created. Linux Mint would also install a working version of GRUB.
- Reboot.

**In Linux Mint**

- Select Linux Mint from the GRUB boot menu.
- Install all updates with `apt update` and `apt upgrade`.
- Make sure every piece of hardware works under Linux Mint. For me I had to install the Broadcom Wi-Fi driver (`broadcom-sta-dkms`) to make Wi-Fi work.

After making sure everything works and suspend is working correctly, reboot.

**In Slackware**

> **[Note]** This part assumes you use ELILO, the bootloader shipped with Slackware as the current default. If you use GRUB or other bootloaders, procedures may differ.

- Mount the Linux Mint drive, graphically or by executing as root `mount <device-partition-name> <folder>` (e.g. `mount /dev/sda5 /mnt/mint`).

  (_From here on I'll assume the Mint drive is mounted on `/mnt/mint`._)

- Switch user to root and change directory to `/mnt/mint`.

    ```
    $ sudo su
    # cd /mnt/mint
    ```

- Copy the initrd and vmlinuz into the appropriate EFI folder. If you've upgraded Linux Mint previously, there may be more than one kernel and initrd; copy the newest version. For me they're with the suffix `5.15.0-47-generic`.

    ```
    # cp boot/initrd.img-5.15.0-47-generic boot/vmlinuz-5.15.0-47-generic /boot/efi/EFI/Slackware
    ```

- Copy the kernel modules to Slackware.

    ```
    # cp -r lib/modules/5.15.0-47-generic /lib/modules
    ```

- Change directory to your EFI folder. For me it's `/boot/efi/EFI/Slackware`.

    ```
    # cd /boot/efi/EFI/Slackware
    ```

- **[Optional]** Rename the Mint initrd and vmlinuz.

    ```
    # mv initrd.img-5.15.0-47-generic initrd-mint.img
    # mv vmlinuz-5.15.0-47-generic vmlinuz-mint
    ```

- Add ELILO entries. Edit `elilo.conf` with your favorite editor (mine is `vim`).

    ```
    vim elilo.conf
    ```

  It should look something like this:
    
    ```
    chooser=simple
    delay=1
    timeout=1
    #
    image=vmlinuz-generic-5.19.6
            label=current
            initrd=initrd.gz
            read-only
            append="root=/dev/sda2 resume=/dev/sda4 vga=normal ro"
    ```

- Append an entry for the Mint kernel and initrd at the bottom of the file.

    ```
    image=vmlinuz-mint
            label=mint
            initrd=initrd-mint.img
            append="root=/dev/sda2 resume=/dev/sda4 vga=normal" 
    ```

  The appended file looks like this:

    ```
    chooser=simple
    delay=1
    timeout=1
    #
    image=vmlinuz-generic-5.19.6
            label=current
            initrd=initrd.gz
            read-only
            append="root=/dev/sda2 resume=/dev/sda4 vga=normal ro"

    image=vmlinuz-mint
            label=mint
            initrd=initrd-mint.img
            append="root=/dev/sda2 resume=/dev/sda4 vga=normal"
    ```
- Save the file and exit.

After installing Linux Mint, GRUB automatically takes precedence over other bootloaders. We need to fix that with `efibootmgr`.

- Run `efibootmgr` for a list of bootloaders.

    ```
    # efibootmgr 
    ```
    
  Output:

    ```
    BootCurrent: 0000
    Timeout: 5 seconds
    BootOrder: 0000,0001,0080
    Boot0000* slackware-15.0+       HD(1,GPT,651aedc3-99fa-6447-b0bf-537afd5c66e6,0x800,0x64000)/File(\EFI\slackware-15.0+\grubx64.efi)
    Boot0001* Slackware     HD(1,GPT,651aedc3-99fa-6447-b0bf-537afd5c66e6,0x800,0x64000)/File(\EFI\Slackware\elilo.efi)
    Boot0080*       PciRoot(0x0)/Pci(0x1c,0x4)/Pci(0x0,0x0)/Sata(0,0,0)/HD(2,GPT,60b362a4-e270-4224-bad3-d5b1345c85db,0x64028,0xe991198)/VenMedia(be74fcf7-0b7c-49f3-9147-01f4042e6842,17a3baf0909199449864a58758401b69)/File(\F3A00EC0-6D2B-4ACB-A25D-3C0BF980C36B\System\Library\CoreServices\boot.efi)
    BootFFFF*       PciRoot(0x0)/Pci(0x1c,0x4)/Pci(0x0,0x0)/Sata(0,0,0)/HD(2,GPT,60b362a4-e270-4224-bad3-d5b1345c85db,0x64028,0xe991198)/VenMedia(be74fcf7-0b7c-49f3-9147-01f4042e6842,17a3baf0909199449864a58758401b69)/File(\F3A00EC0-6D2B-4ACB-A25D-3C0BF980C36B\System\Library\CoreServices\boot.efi)
    ```

  Here we can see the ELILO entry at `Boot0001`. Other entries include a GRUB install at `Boot0000`, and Mac fallbacks at `Boot0080` and `BootFFFF`.

  Take note of the `BootOrder` (`0000,0001,0080` in mine), as it would be used later.

- Make the ELILO entry take precedence. Edit the boot order with:

    ```
    # efibootmgr -o 0001,0000,0080
    ```

  Here I swapped `0001` and `0000` in the previous `BootOrder` value. This varies with every computer.
  
  For example, if your ELILO entry is at `0004`, and your `BootOrder` is `0000,0005,0006,0001,0002,0004,0080`, than you should run:

    ```
    # efibootmgr -o 0004,0000,0005,0006,0001,0002,0080
    ```

- Run `efibootmgr` again to make sure everything is right.
- Reboot.

### Booting the new kernel
- After pressing the power button, spam any letter key (`j` would be sufficient) until the ELILO boot prompt appears.

  The boot prompt looks like this:

    ```
    ELILO boot:
    ```

- Enter `mint` on the boot prompt and press Enter.

At this point, your system should be booted with the Mint kernel. Test if sleep works; if it does, congratulations!

### Bonus: Making the system boot the Linux Mint kernel by default
- Do this as root.

    ```
    $ sudo su
    ```

- Edit `elilo.conf` with your favorite editor.

    ```
    # vim /boot/efi/EFI/Slackware/elilo.conf
    ```

- Move the Linux Mint entry to the top, right below the ELILO settings. For me, before editing it looks like:

    ```
    chooser=simple
    delay=1
    timeout=1
    #
    image=vmlinuz-generic-5.19.6
            label=current
            initrd=initrd.gz
            read-only
            append="root=/dev/sda2 resume=/dev/sda4 vga=normal ro"

    image=vmlinuz-mint
            label=mint
            initrd=initrd-mint.img
            append="root=/dev/sda2 resume=/dev/sda4 vga=normal"
    ``` 

  After editing it looks like:

    ```
    chooser=simple
    delay=1
    timeout=1
    #
    image=vmlinuz-mint
            label=mint
            initrd=initrd-mint.img
            append="root=/dev/sda2 resume=/dev/sda4 vga=normal"

    image=vmlinuz-generic-5.19.6
            label=current
            initrd=initrd.gz
            read-only
            append="root=/dev/sda2 resume=/dev/sda4 vga=normal ro"
    ``` 

- Reboot and test if the Mint kernel automatically boots.

標籤：slackware

2022-11-28
