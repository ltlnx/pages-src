# Microsoft Windows/Office Setup and Activation on (Slackware) Linux

> Update 2024-01-09: Virtualbox 7.0.12 builds with some patches, see
> [this thread](https://www.linuxquestions.org/questions/slackware-14/building-virtualbox-kernel-slackbuild-for-6-6-x-kernel-branch-4175731627/)
> on LinuxQuestions. For the end user using SBo, please wait until Heinz
> updates the Virtualbox-related SlackBuilds.

So first, a disclaimer: The methods I list in this article are solely
for educational purposes, and if you need the software legally, buy a
license, or use free (or better, FOSS) counterparts.

I was listening to a local podcast the other day, and the sponsor of
that episode is a company that sells extension cables and whatnot. As
they were talking about the product, a question arose: when would we
*want to* buy extension cables? If we aren't in need of it, we wouldn't
even think about it. The only moment people really want to buy them, is
when they need the extension cables, but none of them are available. As
a result they utilized this for pushing the holiday discount for the
cables.

In software, just like extension cables, there are some things I miss
only when I need them. One of them is a working Windows virtual machine
setup. I've avoided Windows- or Mac-only software for most of my
university work, but sadly when I deal with classes from the business
and administration departments, I've had less success. Time after time I
encounter pieces of software that only have Windows and/or Mac OS
versions. In those cases, having a Windows VM is pretty handy. (I had to
edit Access databases for schoolwork, and clearly the easiest way to do
that is with Microsoft Access. That prompted this article.)

If you're using Linux, or more specifically, Slackware, here's what I
chose to install, and how I installed them.

## The Actual Tutorial Part

**Preparation**

- Download and install Virtualbox. For Slackware users, that means going
  to the SlackBuild manager of your choice to install
  `virtualbox-kernel` and `virtualbox`.
- Slackware-specific: Read the `README` and `README.Slackware` files,
  and do as they say before you start installing. This is important,
  **especially adding the startup/shutdown routines**, so that virtual
  machines would work.
- Start Virtualbox to make sure it's installed correctly.

**Download Parts**

In this part, we are downloading Windows 10 LTSC, the Long Term
Servicing Channel, to install less bloat in the first place. The Office
version I chose is Office 2016, but feel free to download any later or
earlier versions, though install procedures may differ.

- Go to [the Windows LTSC Download
  page](https://massgrave.dev/windows_ltsc_links.html) on massgrave.dev
  to download Windows 10 LTSC in the language of your choice.
- Go to the "Office MSI VL" link on [this
  page](https://massgrave.dev/genuine-installation-media.html) to
  download Office 2016 in the language of your choice. (If you want to
  download later or earlier versions, the website has instructions on
  how to install and activate them.)

**Installation**

- Open Virtualbox and add a new virtual machine. I'm not going into the
  details since the interface is self-explanatory, but if you're new to
  virtual machines, I'd recommend allocating 40GB of disk space, and 4GB
  or above for RAM.
- **\[IMPORTANT\]** If you're using Virtualbox 6.1.44, there's a bug
  inhibiting mouse activity after going fullscreen, so after you finish
  setting up:
  - Go to Settings → User Interface
  - Uncheck "Show in Full-screen/Seamless"
- Start the machine, and select the Windows 10 LTSC ISO file in the
  popup.
- Run[^1] through the
  installer. For maximal comfortness, disable Internet access for the
  virtual machine by going to the bottom right → right-click on the "two
  screens" icon → click on "Connect Network Adapter".
- After installing, you may want to backup the virtual hard disk in case
  something goes wrong. Here's how you do it:
  - Power off the virtual machine.
  - Go to Settings → Storage and select the one with a hard disk icon.
  - Under "Information" in the right panel, there's the location. Go to
    the location, duplicate the file and give it another name.

**Office Installation**

- Copy the downloaded Office ISO to the machine. There are several
  methods:
  1.  Install Virtualbox Additions by clicking on the relevant option in
      Virtualbox's "Devices" menu, and following the instructions
      on-screen. Then set up a shared folder in the settings.
  2.  For Linux users, open Powershell and use `scp` to copy the file.
      Or just use option 1.
- Double-click on the copied ISO file. This mounts it, and it appears in
  the left pane of Windows Explorer.
- Click on the mounted CD, and double click on the installer (the one
  with the Office icon). Follow the instructions on-screen.

**Optimization**

Here we'll use [a tool by Chris
Titus](https://christitus.com/windows-tool/) to optimize our setup. This
part is optional but if you want a better experience, here's your
chance.

- Remember to re-enable Internet access if you've disabled it in
  previous steps.
- Run the tool as per the instructions on the
  [page](https://christitus.com/windows-tool/).
  - **Do note that the command is the equivalent of `curl | sh` on
    Linux**, so if the site starts showing the slightest bit of malice,
    skip this part and find better tools. Also contact me so that I
    could update this article.
- After the tool opens, apply tweaks as you please. I personally removed
  Edge and set up a delay in feature updates.

The tool installs `winget`, a command-line package manager for Windows.
If you've used package managers on Linux, it should feel familiar. I
installed SumatraPDF, VLC, Librewolf and 7-Zip since they are essential
to replace Edge functionality.

**Activation**

The tool we'll use here is named "Microsoft Activation Scripts", and
it's the single easiest way to activate Windows and Office I've ever
seen. Kudos to the dev(s).

- Follow the instructions to launch MAS from [its
  homepage](https://massgrave.dev/index.html). Again, the warning for
  Chris Titus's tool applies here.
- After the program starts, activate Windows and Office with the
  on-screen prompts. It really is that easy.

**Final Touches**

- If you haven't installed Virtualbox Additions, install it. The
  resize-with-window function is worth it alone.
- Back up the virtual hard disk once more. That way, you can swap in
  this backup disk if anything goes seriously wrong and is not
  recoverable within the virtual machine.

-- ltlnx
2024-01-05


[^1]: Read *suffer*.

標籤：slackware
