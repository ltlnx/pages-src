# 拯救 Slackware 系統大作戰

為什麼會有這篇部落格文章呢？沒錯，又是下錯指令惹的禍。原本想說在 `rc.local` （Slackware 的自訂啟動指令稿）加入刪除 `/tmp` 中部分檔案的指令，結果不曉得是指令下錯還是什麼的，開機時顯示「無法刪除 /boot/efi: 存取遭拒」之類的錯誤訊息，馬上發覺大事不妙，只得用力按住關機鍵強制關機。果然再開機時，系統直接顯示找不到開機裝置。

好險因為前陣子需要重新分割磁碟、讓開機分割區大一點，身邊還備著 AntiX 23 的開機隨身碟。用隨身碟開機後，檢查了一下，系統分割區 `/dev/sda2` 不知道刪到什麼，拒絕裝載；但個人檔案分割區 `/dev/sda4` 還在，而且逛了一下，似乎沒檔案被刪到。

確定個人檔案都還在之後，就是認命重裝系統的時間啦。

## 製作安裝分割區

AntiX 隨身碟中預設安裝了 Broadcom 的 Wi-Fi 驅動程式，因此可以輕鬆連上網路、打開 Firefox 下載 Slackware 映像檔。下載完後，先將 `/dev/sda5` 從 swap 格式化為 ext4（其實這個步驟應該是多餘的，只是做個保險），再將下載好的 Slackware ISO 寫入分割區。但重開機之後一樣找不到開機裝置，我才意識到還有一步沒做：複製 EFI 檔案。

## 複製 EFI 檔案

一般使用 UEFI 開機的電腦都會在第一個分割區尋找名為 bootx64.efi 的檔案，因此需要將 Slackware ISO 的 EFI 檔案先複製到電腦的 EFI 分割區（`/dev/sda1`），才能開機。複製完後重開機，果然看到了 Slackware 開機畫面，也成功開進了安裝系統。

## 重新安裝 Slackware

再來當然是直接執行 setup，但不知道為什麼安裝程式沒辦法成功掛載 Slackware 檔案的所在位置，因此我需要手動下指令

```
mkdir /source
mount /dev/sda5 /source
```

來將分割區先行掛載，再進入安裝程式指定從 /source/slackware64 安裝套件。接下來安裝完套件、設定好網路，再按照 [Slackware Wiki 的指示](https://docs.slackware.com/howtos:slackware_admin:grub_on_first_install) 安裝 GRUB，終於能重新開機了。

## 失去了什麼，留下了什麼

新增使用者 ltlnx、執行 `startkwayland` 開啟 KDE 圖形介面後，意外地發現整個介面跟重裝之前完全相同。當然，自行安裝的套件全部消失無蹤，但不用重新設定圖形介面還是省下了不少麻煩。偉哉現代 Linux 系統。

問題是現在還沒有工具將失去的套件裝回來，因此要先安裝第三方套件管理程式。

## 安裝第三方套件（SlackBuild）管理程式

我慣用的 SlackBuild 管理程式是 `sboui`，一個擁有終端機圖形介面（TUI）的第三方套件管理程式。在前往 [slackbuilds.org](https://slackbuilds.org) 下載 sboui 時，發現自己已經下載過 sboui 與其依賴套件 libconfig 的 SlackBuild 壓縮檔，只要解壓縮再跑一遍 SlackBuild、安裝編譯完的套件即可。因此快速地安裝並設定完了 sboui，準備往下個步驟前進。

## 安裝輸入法

Slackware 預設安裝了 ibus 與 fcitx5 任君挑選，而感覺在 KDE 下運作得比較順暢的是 fcitx5。因此接下來進入 sboui 安裝適用於 fcitx5 的注音輸入法，fcitx5-mcbopomofo 與 fcitx5-chewing。（兩個套件都是我個人維護的，因此對於安裝方式還算熟悉。）在 Wayland 下可以直接從 KDE 設定中選取輸入法，在啟動圖形介面時會自動啟用 fcitx5，因此不需要特別設定環境變數。但為了保險（還有偶爾需要開進 XFCE 的需求），還是得設定一下環境變數。

## 環境變數

除了前述的輸入法設定之外，還需要指定讓程式預設使用 Wayland 的變數。因此需要在 `/etc/environment` 中新增：

```
# set Wayland as default
GDK_BACKEND=wayland
QT_QPA_PLATFORM=wayland
XDG_SESSION_TYPE=wayland

# set fcitx5 as default
GTK_IM_MODULE=fcitx
QT_IM_MODULE=fcitx
XMODIFIERS=@im=fcitx
```

新增完之後重開機，Wayland 應用程式顯示正常，輸入法也能用了。最後就剩下第三方軟體還沒處理。

## 重新安裝常用軟體

這個就比較棘手一點：因為我不可能記得之前安裝的所有第三方軟體，只能憑記憶推敲。平常常用的程式像 LibreOffice、Inkscape、Pandoc、scrcpy、mpv 等隨即浮現在腦海中，缺少的字體如 unifont 也能馬上發覺。剩下想不起來的套件，也只能等到要用的時候再行安裝。

## Bonus：設定 Fontconfig

因為預設 Slackware 的 Fontconfig 會先回退到醜醜的文泉驛正黑，需要多寫 fontconfig 設定才能回退到思源黑體。我在網路上找到了[這個還不錯的 CJK Fontconfig 設定檔](https://gist.github.com/akiirui/b3f36e8bdf9a9f5636a98113960bc7f4)，省得我自己打一個。在將 `53-cjk.conf` 檔案複製到 `~/.config/fontconfig/conf.d/` 之後重開機，字型回退就正常多了。

## 最後一步

上面這樣算是全部設定完了，最後一步就是把暫時用來放安裝檔的分割區重新格式化成 swap 格式，再編輯 `/etc/fstab` 讓 swap 在開機時能自動啟用。格式化完之後，拯救系統大作戰終於圓滿落幕。

## 總檢討

折騰了大概兩個小時，終於把能用的系統裝回來了。就算使用 Linux 好幾年，偶爾還是會難逃一劫，平常就要確保：

- 身邊有 AntiX 隨身碟。AntiX 是真的神奇，什麼驅動程式都有，不用怕開機之後沒網路或是有裝置偵測不到。這樣起碼在出事後有個環境下載映像檔。
- 使用者檔案與系統檔案在不同分割區。這次很幸運，錯誤的指令只有刪到系統檔案、還沒刪到使用者檔案就及時止損。而剛好系統分割區沒辦法掛載，只有在這種時候才能顯現放不同分割區的好處。
- 盡量將設定檔放在使用者下面，然後常常備份。套件可以重新安裝，但設定檔沒辦法，只能重寫。因此像 fontconfig 設定這種能放在使用者資料夾底下的，就放在那裡，這樣只要常常備份，就不用擔心所有設定在一夕之間被重設回到原點。

就這樣。好險有救回來。

-- ltlnx
2024-04-22

標籤：slackware
