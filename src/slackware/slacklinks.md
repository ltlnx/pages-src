# Slackware Links

First of all, this wouldn't be a Slackware Links page without a link to...

[The Slackware Linux Project](http://www.slackware.com/) \
The main site. Parts of the website hasn't been updated for a while (as of 2023-04-17), such as the [General Information](http://www.slackware.com/info/) page which still lists 4.4 as the current kernel version, and the [Other Sites](http://www.slackware.com/links/) page where the now-defunct Slackware Store link hasn't been removed (reasons the Slackware Store was taken down can be found [here](https://www.linuxquestions.org/questions/slackware-14/donating-to-slackware-4175634729/#post5882751)). I still choose to link to it because, well, it's the main site!

### Places to Download Slackware

Slackware has two branches: Stable and -current. The official site only has ISO downloads for the stable version; the -current version is maintained and hosted by Alien Bob on his server (and mirrors). In addition, there are live versions available for download, also maintained by Alien Bob.

For stable ISOs, see:

[mirrors.slackware.com](https://mirrors.slackware.com/) \
The officially-linked mirrors site for downloading ISO images and packages. Click "Slackware ISO Images" on the left pane, choose a version and an architecture (32-bit or 64-bit), then click on the file ending with ".iso" to download the ISO. Remember always to choose the latest version! Previous versions are usually _very_ outdated, and should only be preferred if your hardware doesn't support the latest. (At this point you may consider getting another distro, such as [antiX Linux](https://antixlinux.com/).)

For -current and live ISOs:

[slackware.nl](http://slackware.nl/) \
This is a Slackware mirror plus other goodies, provided by Alien Bob. -current ISOs lie under **slackware > slackware-current-iso** for 32-bit computers, and **slackware > slackware64-current-iso** for 64-bit ones. Do note that for 32-bit computers, there is only a "mini-install" ISO available, which means that you have to grab all packages separately; therefore I recommend downloading the latest stable version then switching to -current mirrors to upgrade.

Slackware Live ISOs lie under **slackware-live > slackware64-_version_-live** for the stable version, and **slackware-live > slackware64-current-live** for -current. All other versions are under the **slackware-live > latest** directory, including 32- and 64-bit Slackware Live -current, and specialized versions for XFCE and MATE desktops, a slimmed-down KDE ("lean") version, and a version for DAWs (digital audio workstations).

### Questions and Answers

Maybe you have trouble installing Slackware. Maybe some peripherals don't work. Or maybe you wake up one day just to find your system not booting. Don't worry; it's likely you can find the solution in one of the following sites.

[Slackware Documentation Project](https://docs.slackware.com/start) \
As the name implies, here lies a majority of Slackware documentation. From installation guides such as [setting up GRUB as the default boot manager](https://docs.slackware.com/howtos:slackware_admin:grub_on_first_install), to more niche topics like [minimizing latency](https://docs.slackware.com/howtos:multimedia:digital_audio_workstation:minimizing_latency) in order to use Slackware as a digital audio workstation, SlackDocs has you covered.

[Slackware Forum](https://www.linuxquestions.org/questions/slackware-14/) \
The official Slackware forum, hosted on LinuxQuestions. Contains all the freshest questions, issues and Slackware lore. A particular thread I like a lot is [this one](https://www.linuxquestions.org/questions/slackware-14/this-is-my-slackware-desktop-725754/page341.html) where people show off their Slackware desktops.

[Slackware Matrix room](https://matrix.to/#/#slackware:matrix.org) ([room chat preview](https://view.matrix.org/room/!txOpDzDRbZXoMPztLz:matrix.org/)) \
If you have an account on one of the [Matrix](https://matrix.org/) servers, you can join this room to discuss about all kinds of Slackware-related stuff. Matrix is kind of a cross-breed of Discord and IRC, where you can join anywhere but enjoy "modern" chat features such as emoji reactions, quoted replies, polls, events and so on.

### Servers

Slackware, like other distros, have a central server that distributes packages, and mirrors that copies all contents of (or _mirrors_) the central server to their own servers, so that people can access the packages more quickly. Here are some of them that mirrors various stuff.

[slackware.uk](http://slackware.uk/) \
A great mirror managed by Tadgy. This mirror hosts the most stuff, including all of Robby Workman's (official) mirror, Alien Bob's mirror, and also additional SlackBuilds like the MATE and Cinnamon desktop SlackBuilds, among others.

[slackware.nl](https://slackware.nl/) \
Alien Bob's mirror.

### Contributer Sites

There are several prominent contributers that maintain a website, some of them being:

[Alien Pastures](https://alien.slackbook.org/blog/) \
Alien Bob is the creator of the Slackware live edition [Liveslak](https://download.liveslak.org/) and the current maintainer of the [Slackware Documentation Project](https://docs.slackware.com/start). He also maintained `ktown`, a series of KDE 5 packages when KDE 4 was the only version official Slackware offered.

This is his blog, where he mostly documents software updates, but sometimes write about [his job](https://alien.slackbook.org/blog/we-are-asml/), [hosting stuff on Slackware](https://alien.slackbook.org/blog/slackware-cloud-server-series-episode-7-decentralized-social-media/) and various [music production-related ventures](https://alien.slackbook.org/blog/configuring-slackware-for-use-as-a-daw/). The comments section of his blog is also one of the most vibrant places to talk about Slackware, aside from the official forum.

[Robby Workman's Homepage](http://rlworkman.net/) \
Robby is the maintainer of XFCE in Slackware. His site is not a blog but rather a loosely-knit web of links, containing HOWTOs, config files, and other stuff. It's pretty interesting nonetheless.

[SlackBlogs](https://slackblogs.blogspot.com/) \
A blog written by Willy Sudiarto Raharjo, the maintainer of [sbopkg](https://sbopkg.org/), [MATE](https://mateslackbuilds.github.io/) and [Cinnamon](https://cinnamonslackbuilds.github.io/) SlackBuilds, and an admin of the [SlackBuilds](https://slackbuilds.org/) project. Mainly contains software update news.

[Pimp my Slack!](https://slackware.ponce.cc/blog/posts/) \
The blog of Matteo Bernardini, the maintainer of a [SlackBuilds repository for Slackware -current](https://github.com/Ponce/slackbuilds). He has an extensive knowledge of Slackware-related stuff and is commonly seen in Slackware forum replies. The blog doen't have much though.

The BDFL Pat doesn't have a blog, and only a [barebones website](http://www.slackware.com/~volkerdi/), but has a Twitter account at `@volkerdi` (though he doesn't tweet that often, and with the current Twitter fiasco going on, I doubt he would anymore). More than often, however, he communicates with the wider Slackware community through forum replies or footnotes in the official [ChangeLog](http://slackware.uk/slackware/slackware64-current/ChangeLog.txt), like this note from March 8, 2023:

> Wed Mar  8 20:26:54 UTC 2023
> 
> Hey folks, just some more updates on the road to an eventual beta. :-)
At this point nothing remains linked with openssl-1.1.1 except for python2 and
modules, and vsftpd. I think nobody cares about trying to force python2 to use
openssl3... it's EOL but still a zombie, unfortunately. I have seen some
patches for vsftpd and intend to take a look at them. We've bumped PHP to 8.2
and just gone ahead and killed 8.0 and 8.1. Like 7.4, 8.0 is not compatible
with openssl3 and it doesn't seem worthwhile to try to patch it. And with 8.2
already out for several revisions, 8.1 does not seem particularly valuable.
If you make use of PHP you should be used to it being a moving target by now.
Enjoy, and let me know if anything isn't working right. Cheers!

### Ending
That's all for now. The list would be updated as I encounter more sites.

標籤：slackware

2023-04-19
