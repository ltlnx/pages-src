# 解決 Pipewire 偵測不到裝置的問題

在 Slackware 下因為沒有正式的依賴套件解決方案，有時 SBo（[slackbuilds.org](https://slackbuilds.org)）會含有 Slackware 本身就有的套件，但版本不一樣，因此會互相覆蓋。Lua 就是一個例子：SBo 中的 Lua 版本為 5.1.5（可能是維護人沒更新、也可能是有套件依賴這個版本），但 Slackware 本身的為 5.4.6，所以在 SBo 安裝套件時要特別小心不要蓋過原有的套件。

上述的 Lua 問題就是這次 Pipewire 沒聲音的罪魁禍首：因為 Slackware 中的 Wireplumber（設定 Pipewire 權限的工具）依賴新的 Lua，但 Lua 被舊的套件蓋過去了，於是 Wireplumber 拒絕啟動。後來把原生 Lua 套件裝回來、重開機之後，聲音又能正常運作了。

謹此紀錄。

-- ltlnx
2024-04-12

標籤：slackware
