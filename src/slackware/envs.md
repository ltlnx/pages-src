# A Possible Fix for X11 Programs Refusing to Start on Wayland

As Wayland and KDE on Wayland matures, I've switched my entire environment to KDE Wayland. Serious props to KDE developers — all of the Konsole and Kwin gliches I experienced in the early days are long gone.

### A short aside: Why Wayland?

As an end user of Slackware, KDE and related software, I find these advantages good enough to make me switch:

- Touchpad gestures. On Wayland three- and four-finger swipes are readily available, while on KDE X11 I'll have to set up stuff like [Touchegg](https://github.com/JoseExposito/touchegg), and still it doesn't integrate as well as KDE Wayland does.
- Correct scaling for mixed DPI situations. I have a Macbook set at 150% scaling, and an external screen set at 100%. Wayland can do that without hacks.
- KDE supporting a global scale for all X11 and Wayland applications across GTK and Qt. Xlib and FLTK aside, no more scaling hacks.

Things like Pipewire and XDG portals are still rough on the edges, and parts of the Wayland stack (libinput) is not as configurable as say, xf86-input-synaptics and synclient, but I'd say the status quo is good enough for laymen like me to make the switch.

Enough rambling, let's get on to the topic...

### What environment variables?

When I started toying with KDE on Wayland (with `startkwayland` in Slackware), I had to run all KDE apps (KDE Gear they call it) under Wayland, otherwise they would look blurry.[^1] Therefore I added the following to `/etc/environment`:

```
QT_QPA_PLATFORM=wayland
```

which makes Qt apps, which most KDE apps are, to use Wayland. Next up is to make GTK apps use Wayland too, so I added:

```
GDK_BACKEND=wayland
```

Also some documentation recommends setting the XDG session type to Wayland too, so:

```
XDG_SESSION_TYPE=wayland
```

Under Qt things are all good: If a Qt app only runs on X11, typically I don't need additional configuration to make them start with Xwayland, the method to run X11 programs on Wayland. However with GTK it's not the case, as when I try to start [Seamonkey]() it errors out:

```
Error: cannot open display: :0
```

I've fiddled with the `DISPLAY` and `WAYLAND_DISPLAY` to no avail, and put the problem aside until I remembered the Wayland environment variables. So I tried `GDK_BACKEND=x11 seamonkey` and sure enough, Seamonkey started up.

### Onto the solution

I've written this helper script to start programs with the correct variables set, named `xenv`:

```
#!/bin/sh
# Set some environment variables to X11

export GDK_BACKEND=x11
export QT_QPA_PLATFORM=xcb
export XDG_SESSION_TYPE=x11
$@
```

Put this in your path and use as `xenv <problematic_app>`.

Have fun!

標籤：slackware

2023-10-25

[^1]: This isn't the case anymore, but still, running with native Wayland makes programs integrate better with the system.
