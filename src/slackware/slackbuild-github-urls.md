# Make Slackbuilds Work Well With Github

While writing a Slackbuild, I encountered a problem where I'm not sure how to work with Github Release tags. The problem goes as follows:

> Hello,
>
> I've encountered a problem when writing SlackBuilds for fcitx5 -- the Github
> releases only has tags, not tarballs. However, tags downloaded from Github
> don't have the package name in the filename. For example, if I want to
> download version 5.0.18 from their "Releases" page by clicking on "tar.gz"
> on the tag, the downloaded file would have the filename "5.0.18.tar.gz",
> as opposed to "fcitx5-5.0.18.tar.gz".
>
> In this case, should the Slackbuild assume the filename
> "fcitx5-5.0.18.tar.gz"? Or should I change it to only assume the version
> number ("5.0.18.tar.gz")? TIA.
>
> Best regards,
> Wen-Wei Kao (ltlnx) 

I posted this to the SlackBuild-Users mailing list, and members Fernando and Alexander promptly replied with this link: <https://slackbuilds.org/GITHUB_URLs.txt>.

The document informs users how to download source files from Github in order to use the SlackBuilds. However this isn't linked anywhere in the main [slackbuilds.org](https://slackbuilds.org) site. So I'm keeping this here for reference.

標籤：slackware

2022-07-24

