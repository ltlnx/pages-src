# New Blog Architecture, a Roadmap

> Edit 2022-05-13: In the end I changed back to a simple list layout,
> for the sake of simplicity and mobile compability. In the future I may
> investigate in tables again and make better use of `@media` queries
> and css `display` options.

I have been writing blogs on Github Pages (now Codeberg Pages) since
2019, and I had always needed to add the sources and links myself,
sometimes even hand-craft the HTML. For the new blog, I wish to blog
completely in Markdown, and have an easy structure for conversion and
the ability to automatically create the "Posts" page.

Given the hirearchy:
```
    Posts/
        無病呻吟/
            0309-test1.md
            0310-test2.md
        科技相關/
            0311-hello.md
            0312-world.md
```
The conversion program should be able to create a table containing links
to all of the posts, in markdown format:
```
  ------------------------------------------------------------------------
               無病呻吟                             科技相關
  ----------------------------------- ------------------------------------
   [test1](無病呻吟/0309-test1.html)   [hello](科技相關/0311-hello.html)

   [test2](無病呻吟/0310-test2.html)   [world](科技相關/0312-world.html)
  ------------------------------------------------------------------------
```
On top of that, I may declare different headings, and introduction text
on the posts page. I may also make new table rows or columns for
clarity. In that case, I wish to have a editable template file, which
has a hook in it to insert converted content; and when doing the
conversion the program will insert the new table hirearchy into the
template, thus creating the new posts page.

All of these should be done in one script that I could run after
finishing a piece of writing. Ideally it would also have a "Beta" hook
which if I include in my source file, it wouldn't parse the file. All
resources should also be detected and put in their respective folders,
relative to the post. Each post should be portable; that is, each post
is a folder and contains the resources it needs, only referring to the
stylesheet.

_Last updated: 2022-05-27_


標籤：隨想
