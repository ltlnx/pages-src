# One Year of Blogging

I just realized that it's been over one year since I scrapped the old site and started a new one, and rolled my own Bash script to convert and update the files. Maybe it's time for a retrospective into the website!

---

### Before I ended up here

When I was in elementary school, something called "class sites" are all the rage: the idea is, every class has a site, where teachers put homework assessments, announcements and whatnot, and students have their personal minisites with a "status line", blog posting functionality, and other social-related activities. That was before megacorp social media sites like Facebook, Instagram and Twitter shook the way online socializing works.

At the time, we are taught basic HTML and CSS in computer class. We learnt about headings, paragraphs, tables and iframes, and basic CSS attributes like `background-color` and `color`, `font-face`, `font-size`, etc., so that we could make a basic site as homework. Looking back, I'm grateful that the class happened and we were taught to write _pure HTML_ instead of plunging into some Javascript framework, so that I know how the building blocks work and not be dazzled by modern web development.

It was about the same timeframe that iGoogle ([remember that?](https://en.wikipedia.org/wiki/IGoogle)) died and I started using Linux, so I grabbed widgets like [the hamster](https://chrome.google.com/webstore/detail/ab-hamster/jnpagnhcemblbjmaodhdeopdaoifdeag) and [the fish](https://chrome.google.com/webstore/detail/ab-fish/afimbdjeknokdnhccecbkonhehohkbpa) iframes, added a clock and made a small "dashboard" site running locally.

In 2018, I opened up my first "blog" on Blogger, mainly for preserving forum posts of instructions that are otherwise hard to find. Stuff like [setting up Internet access for GeeXboX](https://litelinux0.blogspot.com/2018/12/geexbox-network.html), [installing Windows NT 3.51](https://litelinux0.blogspot.com/2018/12/winnt-351-install_8.html) and [Windows ISO links](https://litelinux0.blogspot.com/2019/01/everywindowsdownloadiso.html). The funny thing is, since Blogger has analytics enabled by default, I found out that my posts still get views up to this day. Especially the Windows ISO post, which got 65 thousand views since I posted it in 2019.

In 2021 I posted "New place" (新地方), marking my departure from Blogger. In the post I mentioned the reason for leaving is because Google became a company too large to trust and reason with. Therefore I moved to the next place people think about for "open source blogging": Github Pages.

### Github Pages, SSGs and hand-crafting HTML

<!-- TODO add how SSGs and Github cooperate -->

I've been toying around Static Site Generators, or SSGs, around 2021. The first one I tried is [Hexo](https://hexo.io/), where I had a taste of the horrible NPM ecosystem. It dumps files at all sorts of places, and the files quickly became unmanageable. Then I tried [Hugo](https://gohugo.io/) (which ended up being the first generation of my blog, as seen in [this commit](https://github.com/ltlnx/ltlnx.github.io/commit/963020b9be5615059180dd5438521af28cf45e98)). The build system is a lot cleaner, but the generated content is still too heavy for my taste. I need something easier to maintain, and be more flexible without the concept of "themes".

So I retracted to what I have done since elementary school: write my own HTML and CSS. As seen with [this commit](https://github.com/ltlnx/ltlnx.github.io/commit/a8ed238de2392297104738acbf4c0ccddc31d29b), I deleted everything and added my own homepage, and started writing content in HTML (with `<p>` tags and everything!). At first I tried to make the text vertical since the content was written in Chinese, but ran into all sorts of compability problems including Chrome insisting on starting the page on the left (instead of the right, where vertical Chinese text usually start from, and Firefox gets it right). I changed it back to a horizontal layout and everything seemed good.

There was a problem though — the friction of writing a post is a bit too high. I have to write the HTML, write additional layout CSS for pictures, and update the homepage _manually_. That was a lot of work to do, and it's one of the reasons I don't push out posts on a regular basis. 

In early 2022, I moved the site from Github to Codeberg, because of Github's take on AI training and megacorp stuff. It was about the same time I met [Pandoc](https://pandoc.org/) and its webpage-creating capabilities (with `-s`). Hey, I thought, now I could prepare a folder of Markdown files, run them through Pandoc with a script, then upload them to Codeberg pages. [That's what I did](https://codeberg.org/ltlnx/pages/commit/da5c49ac93f7c6473920902735a1cf0cfb3bcea2) on April 2022: write a script to automate blog building. As time passed I added more functionality, and the script eventually grew into [the 300-line behemoth it is today](https://github.com/ltlnx/ltlnx-blog-updater).

### Codeberg Pages and onward

The site had some build modifications and several CSS revisions, but the format largely stayed the same: a homepage with the first 10 links of a category shown, several category pages, and post content pages. Currently some pages need an update (like the FAQ page, the Blogs I Read page, among others), and I need to embed sitemap creation into the main script, but yeah, I'm pretty content with what I have now. No Wordpress or complicated build systems, only files and a script I wrote myself. Maybe I should consider adding analytics in the Bear Blog "toast" style, so I don't feel so lonely writing and maintaining these pages. However the site will always be as simple as possible, until I no longer maintain the site or give up on the web entirely (one reason may be the [Web Environment Integrity proposal](https://www.theregister.com/2023/07/25/google_web_environment_integrity/)) and move to [Gemini](https://gemini.circumlunar.space/) or something similar.

_Last updated: 2023-08-01_

標籤：隨想
