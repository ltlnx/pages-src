# Quality of Life Improvements

There are some little things in life that don't take a lot of time to
do, and would improve your quality of life significantly after doing it,
but you've never quite wrapped your head around to do it for no reason
whatsoever.

I observed this a lot in my life: I haven't bought a water bottle,
washed my jacket, bought a basket for putting stuff in while taking a
shower, and so on. If I should conclude, they haven't been as large of
an annoyance as to make me buy or do these things.

Other things that affect me more, I've done much quicker, such as buying
a headphone or an umbrella. I just wish I could extend this attitude to
those minor annoyances.

To think of it, I've always been somewhat lenient on my schedule that
doesn't affect anybody else, or isn't that urgent. Some of them require
planning or practice early on though, and I'm trying to improve the
balance so that I won't do things just because I must, but because of a
larger plan or movement I want to achieve.

-- ltlnx
2023-12-18

標籤：隨想
