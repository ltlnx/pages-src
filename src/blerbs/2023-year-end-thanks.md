# Ending 2023, A Quick Recap

Hello dear reader, it's the end of 2023! Which part of the world are you
joining in from? Are there year-end holidays there? In Taiwan we only
have the new year off, which combined with the weekend accumulates to 3
days of holidays. No matter what, I wish you all the best for the coming
year.

In 2023, I've written or updated 38 articles, which was not a lot, but
not too shabby either. Among them there are articles about the blog, my
personal updates (in Chinese), a number of technical writing, and more
recently, some retrospectives on what I've done. There are also plenty
of Chinese articles, before I decided to write in English only for this
blog, and build another place, [notes.ltlnx.tw](https://notes.ltlnx.tw),
for Chinese-only articles.

> Update 2024-03-11: I've stepped back on the English-only decision and
> decided to write new posts mainly in Chinese. The reason behind this 
> is to practice Chinese writing (which is somehow worse than my English
> writing at this point).

There's a running joke about how non-Blogger or non-Wordpress users
write mostly about the technical aspect of their blogs, and I tried to
avoid that in 2023. Fortunately since my blog is built with a method so
basic, there's not much to write about it. That said, I'll definitely
try to expand the topics of my posts to include more music critique,
stories, book reviews, or even interviews if I have the chance.

2023 is also the year I deleted all my Facebook and Instagram friends,
and went all in on the Fediverse. There are a large number of nice
people there, and even if we are in different situations and hold
different values, people on the Fediverse are usually pretty kind to
each other, and I enjoy all the jokes and quirkiness flying around. It's
a choice I don't regret. (Now if I could move off LINE, the notorious
messaging app, in 2024... One can dream.)

If I had to pick one thing I've done that I'm the most proud of in 2023,
I'll say it's the Inkscape translations that [I've just written
about](/inkscape-translation.html). Other stuff that I'm proud of
include arranging the visual identity for a large prom-like event held
by my department, teaching a drum student (watching students improve is
satisfying in itself), and recording numerous song covers (that I've
since taken down from my Instagram account, but the process taught me a
lot about recording and mixing). Missteps also happen somehow often, but
from them I also realize what I'm not good at doing, therefore I see
that as a win too.

I'd like to thank my friends, old and new, for supporting me throughout
the year, goofing around with me and reminding me what I should and
shouldn't do. All of them are talented in one way or another, and I
always look up to them whenever I hit something I'm not accustomed to
do, like breaking the ice with strangers or drawing a bottom line for
myself. I'm thankful that by talking and doing stuff with them, I learn
how to become a better human.

I also want to thank my professors for putting up with me, people on the
Fediverse who interacted with me (or just posted things since I
doomscroll the timelines probably more than I should), everyone who
prayed for me or wished me luck, and most importantly, my parents,
always supporting me for whatever I do.

I'm looking forward to 2024, where I hope I could calm down and study
more, but if something extraordinary happens and plans change, I'll deal
with it. Cheers.

--ltlnx
2023-12-30

標籤：隨想
