# Follow Twitter Users on Mastodon (or any ActivityPub implementation)

> Update 2024-04-21: bird.makeup doesn't work as of now, because Twitter
> changed their API and the alternative API bird.makeup was using, Nitter,
> stopped updating some weeks ago.

One day on Hacker News I found out the [bird.makeup](https://bird.makeup/) site, which makes bot accounts of Twitter users to make them followable on ActivityPub implementations like Mastodon. If this sentence means nothing to you, let me explain some of the words:

- Twitter is a once-popular microblogging (a place to write short-form texts) service. It is in a continuous demise since the entrepreneur Elon Musk acquired it and screwed the platform.
- ActivityPub is a standard that allows different web services to interact with each other, and implements common social network activities such as following, liking, announcing, adding and blocking. A number of web services implement this protocol, like
  
    - microblogging platforms Mastodon, Plemora and Misskey (think Twitter),
    - media hosting sites Peertube (think Youtube), Pixelfed (think Instagram), and Funkwhale (think Spotify),
    - blogging platforms like WriteFreely (think Blogger or Bear Blogs),
    - and other services made for different use-cases.
  
  The nature of ActivityPub makes it possible to say, follow a Peertube account on Mastodon, which is unimaginable on other individual platforms.
- Mastodon is an implementation of ActivityPub that focuses on Twitter-like microblogging. People can set up "instances" of Mastodon with different rules, such as
  
    - which servers to "federate" (recieve and send content) with,
    - what types of content are allowed in the instance (like R18 regulations),
    - and what kinds of people are walcome on the instance in general.
  
  There are a range of instances that consist of different political views, regions or mindsets, and we can choose which public instance to join, or whether to host our own instance.

I don't know whether that explains it correctly; if not, there are definitely other people explaining these better.

So on to bird.makeup...

### How the site works
The site is as easy as it gets: type in the Twitter handle of whom you want to follow, and it will give you a corresponding `@<account>@bird.makeup` account. Open up your Mastodon (or Plemora, et al.) account and follow the bird.makeup account there. That's it: you can now see all the account's Twitter posts, in the comfort of your instance's "Home" or account timeline.

### Why is this interesting
Well, you can follow anyone on Twitter _without them knowing it_. That's the interesting point about this service and the Fediverse in general: if the server doesn't federate with your server, you can piggyback on their posts and reply all you want without their knowledge. It's the modern version of talking behind people's backs.

This can raise problems such as commercial instances piggybacking on posts to serve ads, and lead to instance moderators blocking them, which in turn raises the burden of instance moderators. Although this might not be a problem for now, I see it being a huge issue in the future, as the Fediverse keeps growing.

But in the meantime, Twitter isn't going to implement ActivityPub and federate, so I dunno, just enjoy the service I guess :)

_Last updated: 2023-03-23_



標籤：隨想
