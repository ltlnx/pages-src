# Disclosure on the Influence of Opinions

Recently I came across [Damien's Blog](https://damieng.com/) because of a font he created — [Envy Code R](https://damieng.com/blog/2008/05/26/envy-code-r-preview-7-coding-font-released/). The font itself is a good mixture of old and new, with Terminus-style boldfaces (which I like _a lot_ and feel like too little monospaced fonts does this), and a generally pretty clean appearance. Sadly the font was still in beta, and has not seen progress since 2008. As the comments on the page stated, it would be appreciated if new versions can better distinguish between similar characters (0/O, I/l/1/|). But I digress.

![Font showcase on Damien's website. Retrieved 2022-6-28. [Original](https://images.damieng.com/blog/EnvyCodeR-PR7-Humane.png)](res/EnvyCodeR-PR7-Humane.png)

His website has a lovely section named "[Site details & disclosure](https://damieng.com/about/responsible-disclosure/)", which states the technologies used on the website, and disclosure on experience that may have influenced his opinion. This is a great practice in my opinion, as it gives readers a better understanding of why some opinion is expressed in some particular way.

On the website he has the following sections:

- Technology
- Hosting
- Licensing
- Responsible disclosure

In the "Responsible disclosure" section, he listed his previous employers, software he used or participated in, affiliates and advertising, and "everything else" (gifts for example). I like the transparency this gives to the readers. I may add these to my [About](../others/about.html) page some day.

_Last updated: 2022-06-28_


標籤：隨想
