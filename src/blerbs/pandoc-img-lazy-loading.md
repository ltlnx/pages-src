# 將 Pandoc 處理的 Markdown 圖片套上 lazy-loading 屬性

在寫上一篇 [Haiku Beta R5 試用](/haiku-betar5.md) 的時候，因為圖很多、不想影響載入速度，於是搜尋起了如何讓 Pandoc 在轉換 Markdown 的時候套上 lazy-loading 的屬性。於是找到了這篇 Github Issue：[HTML5 feature: lazy loading images](https://github.com/jgm/pandoc/issues/6197)，其中 jgm（Pandoc 維護者）提到，只要使用 3 行的 Lua 過濾器就能輕鬆達成這個目的：

```
function Image(el)
  el.attributes.loading = "lazy"
  return el
end
```

將這段程式碼存為 `lazy.lua`，並在執行 `pandoc` 時使用：

```
pandoc --lua-filter lazy.lua
```

就能將所有圖片套上 lazy-loading 屬性。在實際轉換文件時這會讓速度稍微慢一些。

-- ltlnx
2024-09-16

標籤：隨想
