# Languages, Translation, and Blogging

First of all, I definitely should blog more, to push more thoughts into the wild and reiterate on what I've read or done. The problem is: which language should I do it in?

You see, in general I like learning new languages and using them. My native language is Chinese (Taiwan), and I've used English since I was a child. In high school it was mandatory to choose another language to learn, so I learnt a little French. Now in university, I'm in a department mainly teaching European languages, and I've been learning Spanish for three years.

However, the current way I implement index page creation does not allow the same article to exist in different languages. If I were to write an article in English and translate it into Chinese, they can only exist on different posts (or on the same post, but I doubt anyone wants that). That said, what if I implement a way to mark different languages and incorporate them into the index pages? A crude process would be:

- I create two files, `title.zh.html` and `title.en.html`, with translations of the same post.
- When converting they get different HTML language tags, and links to one another.
- The bash script interprets it and shows `Title ([中](title.zh.html)|[EN](title.en.html))` in index pages.
- The title text shown depends on the index page language. (So I have to implement it too, lovely)

It looks pretty difficult to implement, but _if_ I've done it, another problem emerges: Do people really read or like multilingual blogs? I mean, there must be a reason why there are so few blogs that are multilingual, right? Even some of my friends in Taiwan (Internet friends, admittedly) only choose one language between English and Chinese, and keep posts in other languages on other blogs or elsewhere.

That's my struggle. Sometimes I think in English (for tech stuff) and sometimes I think in other languages, therefore the language of my first draft usually depends on which mode I'm in. After that, I have to choose between translating the draft into another language or just keep it, and judging from the things I post, you can see that I chose the latter. The problem is, I've always wanted to write mainly for Chinese readers, and keep their English variants as a side reference. (That is, if I have readers at all, since I have zero analytics honestly I don't know.)

To conclude this, I may add proper multilingual support if I have time, but in the meantime it will stay as-is.

_Last updated: 2023-10-02_

標籤：隨想
