# Inkscape Translation Contribution, and Other Short Stories

As the title stated, my Traditional Chinese translations are included in
Inkscape 1.3.1 and onwards. This is my first serious contribution to an
open source project, and I'm pretty glad seeing my work actually applied
onto the software. I hope they will make the experience better for
Traditional Chinese Inkscape users. But how did I come to use Inkscape
in the first place, and how did the translation process go? Here's some
notes...

## So I started using Inkscape

I've heard of Inkscape a long time ago, but never got around to use it,
because I was more content with programs like GIMP and Krita to do
design work. To be honest, Inkscape (and other vector editors) scared me
with the initial learning curve. As luck would have it, I'm a bit burnt
out around June 2023 by all the drumming gigs I did in May, and tried to
find other things to do, so I decided to try Inkscape for my next
graphic design commision for a summer camp. It turned out to be a great
decision.

At first I watched various tutorials on YouTube to grasp how shapes,
node editing and text wrangling work in Inkscape, and was attracted to
the "Text on Path" feature --- I can't believe I missed out this the
whole time on other image editors. After doing more work and trying out
features like vectorizing photos, laying out documents, and creating
letterings, I'm completely sold on Inkscape. However my problem with the
software lies in the state of Traditional Chinese translations.

## Translations and UI

The Traditional Chinese translations of Inkscape at the time was, let's
say, lacking. There were visible untranslated parts everywhere, namely
in the Align and Distribute dialog, the dialog tab names, parts of the
menu items, in the settings, and in the tooltips. My gripe was that some
of them were *partially* translated, meaning that maybe the tool name
was translated but the tooltip wasn't, or in the case of the Align and
Distribute dialog, some align options were and some weren't, even though
they are similar functions.

To be fair, before I jumped in and started translating, I didn't know
that some parts look untranslated because the source in English changed
subtly, rendering the previous translation obsolete, therefore not
showing up in the interface.

So I wondered who can translate the interface, who did the translations
previously, and as it's an open source project, if I could somehow help
with the translations.

## Open Design Club, and ties with the community

Enter Open Design Club, a school club I joined in September 2023 thanks
to the recommendation of a professor teaching open source design
software as part of the liberal arts courses in my university. I joined
the club and found out that they mainly use and teach Inkscape, albeit
in a pretty amateur way.

One day I chatted with the professor about translating Inkscape (and the
[manuals](https://inkscape-manuals.readthedocs.io/en/latest/)), and to
my surprise, he knew a person doing translations for Inkscape's web
interface. We got in touch, and he told me how translations work for
open source software, tips on contributing to them, and the basics of
translation, such as how to choose the right translation for words.

After that, I searched for how Inkscape manages its translations, and
found out about the [inkscape-translators mailing
list](https://lists.inkscape.org/hyperkitty/list/inkscape-translator@lists.inkscape.org/).
I sent a message regarding how website translations work (I wanted to
translate the Inkscape Beginner's Guide first at the time), and the
translations handler, [Moini](https://inkscape.org/~Moini/), responded
pretty promptly, and we went on Inkscape's GitLab to discuss further.
After that Moini suggested me to join [Inkscape's RocketChat
instance](https://chat.inkscape.org/), so I created an account there.

## Inkscape RocketChat, and the 20th anniversary

The Inkscape Rocket.Chat instance is a pretty lively instance with
chatrooms discussing various aspects of Inkscape, such as the website,
translations, development, marketing (vectors), user experience,
testing, and a general user chatroom where people post quick questions
and depending on whether anyone knows the solution, someone responds.

Since practically most of the Inkscape behind-the-scene discussions
happen there, I feel pretty privileged as I can join any of the
chatrooms, and simply lurk there or participate when needed. I can see
what the developers or other people involved are up to, and how the
development and marketing of the program work. I like the transparency
of the teams in particular.

While I was lurking around, I found out that developers and "vectors"
(I'll use the word without quotes for the rest of the article, in
Inkscape parlance vectors means marketing people) were preparing for a
celebration party for the 20th anniversary of Inkscape. People
volunteered to host and respond to comments for the party, and planned
out how the party would go. Moini made a presentation with collected
wishes from the community, Lazur (or Pacer?) created the artwork used as
the background, Martin and Michèle (and others I forgot) opted to host
parts of the party, and Marc provided the hosting infrastructure.

And it worked! The party was a huge hit, with people sharing their
experiences with Inkscape, expressing gratitude, the hosts reciting the
community wishes and everyone simply hanging out and chatting together.
The feeling was magical: we were all in different timezones and speak
different languages, yet everyone were having fun in the party, and it
felt like a *real party* rather than a meeting or such, which in my
opinion is the hardest thing to get right for such online parties. In
conclusion, it was awesome and I'd totally participate in the next.

## Moving on

After the party and launching Inkscape 1.3.1, my translations landed and
I talked some close friends into updating (Windows users especially
aren't accustomed to updating their software manually, hence web
browsers usually set up automatic updates), and they liked how much more
native it feels with a translated interface in place. Sadly 1.3.1 had a
major bug preventing shapes from showing up in web browsers, but devs
fixed it pretty quickly by publishing 1.3.2 one month later. In the
small release window I squeezed in a bit more translations too.

To conclude, the experience contributing to Inkscape has been
overwhelmingly positive, and I like the fact that major developers of
the project are also answering questions of the community, making the
software not ice-cold like corporate software, but a piece of
collaborative work with real people behind it. It feels more intimate,
if that makes sense.

-- ltlnx
2023-12-20
