# 中英夾雜文書的空格處理

中英文夾雜的文字在閱讀上，為了將中英文格開來，往往會在英文的兩旁加入空格；但遇到標點符號時，因為標點符號旁本來就有空格，就不需要特別加入空格。以這段文字為例：

> netlify還有很多功能，像是加入https，讓網站和github結合，有push的時候自動出發CI及CD，有pull request的時候自動建立出一個preview網站等。 

_（取自 [Alan Tsai 的學習筆記](https://blog.alantsai.net/posts/2018/07/migrate-blog-to-ssg-demo-devops-8-netlify-free-static-site-hosting-service)）_

加完空格的版本看起來如下：

> netlify 還有很多功能，像是加入 https，讓網站和 github 結合，有 push 的時候自動出發 CI 及 CD，有 pull request 的時候自動建立出一個 preview 網站等。 

當然，這段文字如果是我寫的，我還會把專有名詞的首個字母大寫：

> Netlify 還有很多功能，像是加入 https，讓網站和 Github 結合，有 push 的時候自動出發 CI 及 CD，有 pull request 的時候自動建立出一個 preview 網站等。 

因為偶爾還是會看到沒加好空格的網站，在這裡筆記一下。

_Last updated: 2023-04-17_


標籤：隨想
