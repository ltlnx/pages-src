# Android 應用程式推薦

截至文章發表之時，Linux 在行動裝置上還是有許多介面瑕疵與硬體支援問題，但隨著市面上推出專門跑 Linux 的行動裝置如 [PinePhone Pro](https://www.pine64.org/pinephonepro/) 與 [Librem 5](https://puri.sm/products/librem-5/) ，只要軟體跟進，好用的 Linux 手機不是夢。但在這之前，市面上還是以 Android 跟 iOS 手機為主。而 Android 的生態系又比 iOS 開放一些，因此我比較喜歡用 Android 手機當作主力機。以下是我使用過，推薦或不推薦的 Android 程式。

### 手機本體
我目前的手機是 Sony 的 Xperia 10 IV。這是市面上所剩無幾，擁有正常配置的手機之一：有 MicroSD 卡插槽、耳機孔、指紋辨識、LED 指示燈，而且沒有醜醜的瀏海。更棒的是，它擁有 5000 mAh 的電池，也就是說在充飽的狀態下，用個兩天不是問題。再者，相對於其他廠牌，Sony 手機很容易就能解鎖、安裝 Magisk，是改機、安裝各種外掛的好選擇。

可惜，這台手機出廠時，許多基本功能是由 Google 應用程式擔任。而我討厭 Google 已經不是一天兩天的事了，因此我拿到手機後，第一步就是安裝應用程式來取代它們。但在這之前，我會先安裝拿來取代 Google Play 的應用程式商店——

### 應用程式商店
- **F-Droid**：這不用多作介紹，是每個有心安裝開源應用程式的人必裝的應用程式商店。安裝的方法是：
  
    - 打開預裝的 Chrome（這是我們最後一次打開 Chrome 了！），前往 [f-droid.org](https://f-droid.org/) 點選 "Download F-Droid"。
    - 點一下下載好的 apk 檔，允許 Chrome 安裝來路不明的應用程式。（安裝程式前還是需要我們點選「安裝」，所以不用擔心資安問題。）
    - 點選「安裝」安裝 F-Droid。安裝好之後 F-Droid 的應用程式圖案會出現在主畫面上。
  
  打開 F-Droid，等待應用程式資訊更新完畢後，就可以安裝另一個取代 Google Play Store 的應用程式商店：
  
- **Aurora Store**：這個程式能讓我們在不用 Google 帳號的情況下安裝 Google Play Store 上的應用程式。直接在 F-Droid 上搜尋安裝就可以。

接下來所有推薦的應用程式後面，我會標明是從哪個應用程式商店安裝的。下一步來安裝一些取代基本功能的應用程式。

### 檔案瀏覽器
- **Material Files** _(F-Droid)_：一個介面乾淨，功能齊全好用的檔案管理器。
- **ZArchiver** _(Aurora Store)_：一個能壓縮、解壓縮各種檔案的壓縮檔管理器。一樣是介面乾淨好用。

### 網路瀏覽器
在解除安裝或停用 Chrome 之後，我們需要安裝其他網路瀏覽器來瀏覽網站、搜尋東西等等。在安裝瀏覽器之前，可以先安裝一個管理瀏覽器更新的應用程式：

- **FFUpdater** _(F-Droid)_：用來安裝各種瀏覽器、管理各種瀏覽器更新的工具。會需要這個工具的原因是，許多瀏覽器的更新只有在網路上發布，並不會發布在 F-Droid 或 Play Store 上，因此有個工具管理起來會比較容易。

安裝完成、打開 FFUpdater 後，就能輕鬆地安裝以下瀏覽器：

- **Iceraven** _(FFUpdater)_：以 Firefox 為基底，但包含更多外掛程式（像是拿來防止網路追蹤的 ClearURLs）。
- **Mull** _(F-Droid、FFUpdater)_：同樣是以 Firefox 為基底，但預設開啟更嚴謹的保護措施，同時關閉了所有回傳資料給 Firefox 母公司 Mozilla 的設定。
- **Bromite** _(FFUpdater)_：以 Chromium（Chrome 的開源版本）為基底，移除所有回傳資料給 Google 的設定，並內建廣告阻擋器。

同時，在 F-Droid 上也有一些滿足不同需求的瀏覽器：

- **Privacy Browser**：以 Android 內建的瀏覽器 Webview 為基底，加入許多保護隱私的功能，如 Javascript 開關、Cookies 開關；並增加一些只有在電腦瀏覽器會有的功能，如查看原始碼、字體大小，以及切換 User Agent（模擬各種瀏覽器）的功能。

### 音樂播放器

- **AIMP** _(Aurora Store)_：唯一推薦，超級好用的音樂播放器。可以自定不同的播放清單，預設是以資料夾跟檔名作排序，只要把音樂檔名打好基本上就沒問題了，不用像其他播放器還要將歌曲資訊輸入進去才能作排序。更棒的是它內建 EQ 等化器，如果想要不同的聆聽體驗，只要載入不同的 EQ Preset 就可以。

### 相機

- **Google Camera (GCam)** _(網路下載)_：是能讓中階機拍出高階手機效果的相機。比較麻煩的是，因為每台手機有不同的相機設定，所以要上網找最適合的 GCam 改造版安裝使用。通常只要搜尋「（手機機型） gcam xda」就可以找到關於最適合你手機的 GCam 的相關討論。
- **Gcam Services Provider (Basic)** _(F-Droid)_：如果你使用的是 Custom ROM，可能沒辦法直接跑 GCam，因為 GCam 需要 Google Play 服務才能執行；但這個時候只要下載這個程式，它會模擬所需的 Google Play 服務，就不需要特別安裝 Google 服務框架了。

### 影片播放器

- **NewPipe** _(F-Droid)_：NewPipe 是一個拿來看 Youtube 影片、下載 Youtube 影片的利器。除了看 Youtube 影片，它還可以拿來聽 Youtube Music 中的音樂，還可以下載影片或音樂的音檔，是集 Youtube Premium 與 Youtube Music Premium 功能於一身的強大程式。
- **LibreTube** _(F-Droid)_：與 NewPipe 功能基本上相同，不過多了能看影片留言回覆的功能。但對 Youtube Music 的支援似乎不怎麼好（我很少用這個程式）。
- **VLC** _(F-Droid)_：拿來看剩下各種影片的影片播放器。好處是能支援各種影片格式。

### 字典

- **Livio 的各語言詞典** _(Aurora Store)_：Livio 出品的字典都是以該語言命名，如 English、French、Spanish 等。它將維基詞典 Wiktionary 的各語言詞典包裝成離線可使用的程式，而且介面乾淨、無廣告。

### 通訊軟體

通訊軟體是我的一大痛點，因為最常用的是最討厭的其中幾個。

- **Element** _(F-Droid、Aurora Store)_：運用 Matrix 通訊協定，端到端加密、聯邦式（Federated）的通訊軟體。整體介面蠻慢的，訊息同步的速度也是（但依照各個伺服器可能有所不同），但該有的功能都有。要說缺點的話，就是設定過程偏繁瑣，因此很難說服家人朋友使用。
- **Telegram** _(F-Droid、Aurora Store)_：真心推薦的通訊軟體。介面快速用心、各種好用的功能都有，而且沒有太多惱人的廣告（只有在公開群組中會偶爾出現）。比起下面幾個程式好太多了。
- **Messenger Lite** _(Aurora Store)_：沒有廣告的 FB Messenger。雖然少了回覆特定訊息跟顯示訊息回覆的功能，但整體用起來還可以。這類預設不加密的通訊軟體我比較喜歡 Telegram，但因為大家都在用所以只好繼續用。
- <span style="color: red">**LINE**</span> _(Aurora Store)_：通訊類程式的反指標——
    - 它的安裝檔很大，非常的大，而且分成好幾個子 APK，所以從網路上下載還不能一次安裝。（最近好像又可以了）
    - 滿滿的，**滿滿的**廣告跟不必要的功能。
    - 把所有功能包在一個程式裡面，如 LINE Pay、Voom、LINE Today 等。分開成各自的程式不是很好嗎？
    - 啟動速度是我用過的通訊軟體裡面最慢的，大概要 5–10 秒。這支手機甚至是三年內的中階機。
    - 沒有 Google Play 服務，或是沒給 Google Play 服務網路權限，就收不到訊息通知。Telegram 能開啟另類接收通知的管道，Element 跟 Messenger 沒這個問題，但 LINE 什麼都沒有。
    - 幹掉了 LINE Lite。曾經有個類似 Messenger Lite 的東西，能拿來在比較低階的手機上收訊息，而且功能比較少（這是好事），但 LINE 在 2022 年 2 月停止了 LINE Lite 的服務。
    - 各種介面上的問題：
        - 底部的導覽列不能自訂，所以我用不到的功能佔了快要一半（LINE Voom 跟 LINE Today）
        - 如果要把聊天室加到主畫面，要去聊天室理的設定 → 其他設定 → 將聊天室加到主畫面（為什麼不能長按聊天室，然後直接加到主畫面呢？）
        - 在聊天頁面搜尋，也會搜尋各種 OpenChat、官方帳號、貼圖跟主題。（你首頁也放一個搜尋框是放心酸的？或是說，為什麼不能把搜尋分開，聊天頁面只搜尋聊天紀錄、首頁只搜尋朋友，剩下的搜尋再放到貼圖或主題小鋪之類的？）
      還有太多太多小小的缺失，導致整個介面用起來會讓人發火。
    - 別的通訊軟體做得到的，抱歉這裡沒有：
        - 其他所有通訊軟體都能看到誰讀了訊息，唯獨 LINE 沒有。
        - 在訊息上左滑回覆。Telegram 跟 Messenger 都有這個功能。 _（註：最新版本有這個功能了）_
        - 將聊天室群組分類。Messenger 跟 Element 也沒有但 Telegram 有。
        - 無限期下載圖片、檔案。連廣告比較少的 Telegram 都辦得到了，我真的不懂為什麼 LINE 七天就要過期。
        - 沒辦法直接把檔案存到手機裡，只能檢視。
        - 在不同裝置移動會讓訊息全被刪掉，而且訊息備份只能連通 Google Drive。同樣是端到端加密，Element（Matrix）就能用密鑰的方式同時保有訊息加密又能留住訊息，不知道為什麼 LINE 做不到。
    
    我留著這團燃燒中的垃圾純粹是因為大家都還在用，不然早在 LINE Lite 停止服務時就該刪了。

### 地圖
- **Google 地圖** _(內建)_：不得不說 Google 在這方面還沒有勁敵，至少在導航方面是挺準確的，而且不用登入 Google 帳號、不用啟動 Google Play 服務就能使用，所以至少這個程式我是沒什麼敵意的。
- **Organic Maps** _(F-Droid)_：一個可能未來會取代 Google 地圖在我心目中地位的程式。介面快速好用、資料還算準確，不過還是有 OpenStreetMap 的通病——沒辦法搜尋門牌號碼，不然就完美了。

### PDF 檢視器
不知道為什麼，PDF 檢視器不是預裝的程式。至少在對面戰場（iOS）它是預設就有的。

- **Librera FD** _(F-Droid)_：堪用的 PDF 檢視器，雖然介面有些老氣，不過能完成 PDF 閱讀器的本分。同時也能開 docx、epub 檔。

### 日曆

- **Simple Calendar** _(F-Droid)_：很基本的日曆。基本功能都有，介面乾淨整潔。

但如果要管理網路上的日曆（如 Google 日曆或 Nextcloud 日曆），則需要以下的程式：

- **DAVx5** _(F-Droid)_：管理網路日曆的工具。設定起來跟電腦上差不多，不會太複雜。網路上也有加入 Google 日曆的教學。

### Office 軟體
如果有在手機上檢視或編輯 Office 檔案的需求，可以試試看這些程式：

- **Microsoft Office** _(Aurora Store)_：說真的其實官方的 Office 程式體驗並不會太差。而且如果在這裡登入，就可以直接存取 OneDrive 上的檔案，對於 Windows 使用者應該是不錯的選項。
- **Collabora Office** _(網路下載、F-Droid 自定軟體源、Aurora Store）_：開源類 Office 程式第一首選。因為基於 LibreOffice，有些文件開起來可能會有點跑掉，但基本文件的支援度蠻好的。
- **Office HD** _(Aurora Store)_：在電腦上叫 SoftMaker Office，功能跟電腦版相同，因此手機上用起來可能有些拘束，平板上就沒問題；同時是這幾個軟體中，唯一支援滑鼠右鍵的程式。如果你用的是 Android 平板，可以考慮看看。

另一個我沒用過，但電腦版還不錯的：

- **OnlyOffice** _(Aurora Store)_：在寫這段的時候才想到 OnlyOffice 似乎有出 Android 版，但因為沒用過所以不方便評論。電腦版倒是真的不錯，是 MS Office 檔案支援度最高的一款。

### 筆記軟體
筆記軟體有分兩種，純文字跟非純文字筆記。純文字就是像 Simplenote，純粹只能用文字，頂多再加個勾選清單；非純文字就是像 Notion，能放圖片跟各式各樣的東西，文字也能加粗、打斜等等。

純文字：

- **Standard Notes** _(F-Droid)_：曾經很推，現在不太推的筆記程式。是這裡面唯一有端到端加密的筆記程式（所以能存密碼什麼的），但程式大小越來越大、啟動越來越慢，而且加入越來越多付費解鎖的功能，總之不是我想看到的發展。
- **Simplenote** _(Aurora Store)_：優點是啟動速度快、不會打斷思緒，而且擁有「分享到網路上」功能——按下去就能複製網址，讓其他人在瀏覽器上看到這篇筆記；缺點是沒有端到端加密。

非純文字：

- **Notion** _(Aurora Store)_：應該不用我介紹，是許多人在用的筆記軟體。優點是功能多，缺點是速度超級慢，慢到會讓人心情不愉快。

### Podcast 播放程式
- **AntennaPod** _(F-Droid)_：能連通 iTunes Podcast 的神奇軟體，因為能連通 iTunes 所以幾乎什麼都聽得到。

### QR Code 掃描器
- **Binary Eye** _(F-Droid)_：就是 QR Code 掃描器。沒有廣告，沒有奇奇怪怪的功能。好用。

### 鍵盤
- **Google 注音輸入法** _(網路下載)_：Google 為了推行 GBoard，停掉的鍵盤程式。好打，選字大多正確。
- **LIME** _(網路下載)_：如果要打行列或是其他碼表輸入法，LIME 比其他市面上支援碼表的鍵盤（如 gcin）都還要好用。可惜作者不再更新，但截至 Android 12 這個程式還能用。推薦給需要的人。
- **樸實注音輸入法** _(F-Droid)_：由電腦上的 Chewing 輸入法移植而生，優點是支援倚天 26、許氏鍵盤鍵位，缺點是需要打出聲調，不像其他手機上的注音輸入法。

### 其他
- **Save To...** _(F-Droid)_：能繞過一些白癡程式（如 LINE）沒辦法下載檔案的限制，能假裝是 PDF 閱讀器、Office 編輯器等能開啟檔案的程式，實則將檔案下載下來，存到指定的位置。
- **Termux** _(F-Droid)_：能在 Android 上用的 Linux Terminal。同時擁有軟體管理器，能串連 apt，用類似 Debian、Ubuntu的方式管理程式。是想在 Android 上跑 Linux 程式的救星，極力推薦。
- **Easy Noise** _(F-Droid)_：白噪音產生器。
- **Simple Recorder** _(F-Droid)_：相當於 iOS 的「聲音備忘錄」。
- **XScreenSaver** _(F-Droid)_：一些酷酷的動態桌布。
- **KDE Connect** _(F-Droid)_：使用 Linux 與 KDE 的朋友會愛上的一個程式。只要連在同一個網路上，就可以互傳檔案、分享剪貼簿、控制對方的媒體播放，還能當作電腦的滑鼠使用（以前還能當鍵盤，不知道為什麼這個功能不見了）。

目前只想到這些，這個清單會隨著時間改變。

_Last updated: 2023-04-10_

標籤：隨想
