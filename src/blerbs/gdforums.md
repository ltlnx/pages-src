# 開源設計相關論壇

因為今天又看到了幾個論壇（也辦了帳號），想說寫一篇文章把這些論壇放一塊。

- Inkscape 官方論壇：<https://inkscape.org/forums/> \
  就是 Inkscape 的官方論壇。每月挑戰會在上面舉辦，也可以貼問題、作品到上面，但整體來說就是個普通論壇。
- Krita 畫家論壇 (Krita-Artists)：<https://krita-artists.org/> \
  Krita-Artists 的設計很特別，他們的作品頁面有特殊排版能彰顯作品本身，每月的挑戰也有得獎作品的展示區在主頁面上方的區塊。整體來說是個認真經營的論壇，但看起來是有 5-6 個人每天悉心 moderate 而成。
- Pixls.us 論壇：<https://discuss.pixls.us/> \
  開源照相與影像處理的論壇。裡面討論的主力是 GIMP、Digicam、Darktable 與 Rawtherapee。

-- ltlnx
2024-09-27

標籤：隨想
