# Refocusing

The thing is, in 2023, I didn't really *have* a focus, and as a result,
I did a pretty wide range of stuff: I played multiple drum or other
instrument gigs, participated in a business competition, did a bit of
live PA (and left the PA club in my university due to my poor time
management), held multiple titles in different clubs (in the rock n'roll
club as a gear manager, and in the Open Design club as a website
maintainer), been a graphic design and drum teacher, and did quite a bit
of graphic design for events in my department.

Not to say that I've regretted anything I did, most of them worked
somehow; but I wish to focus more on my studies and actually learning as
a university student. To do all the things I mentioned above, I dropped
coursework, skipped classes, and in general ignored what I should do as
a student, but the thing is, I'm paying (OK, my parents are paying for
the time being) for the classes, and I feel like I'm not spending the
required time and energy on my major in university.

On the other hand, I've mostly neglected reading and doing research on
whatever I'm doing, like graphic design and programming. The current
process goes like this: I get caught in a project I've participated in,
jump in and do things blindly, and by the time all tasks finished I find
myself caught in another project, then the cycle repeats itself. What I
wanted to happen is that I go into projects *knowing what I'm doing*,
instead of grinding myself through the process and forgetting how I did
them later.

## What's your plan then?

In the next year, I'll refrain from agreeing to do too much music gigs
or projects just because I feel like I should support my friends or
something. Instead, I hope to study and do more research beforehand, and
try to be knowledgeable enough to be confident in what I'm doing. I'll
also focus on being more professional: be on time, improve my
communication skills, charge money for services, and so on.

Goals of 2024:

- Try my best to get as much credits as possible to finish my degree on
  time.
- Be professional on my new sysadmin job (starting in January). It's not
  paying much but seems fun, and I hope to learn a lot by helping to
  manage school server infrastructure.
- Continue contributing to Inkscape and other projects, in areas I'm
  able to. Currently I'm doing translations and introducing Inkscape to
  Traditional Chinese people on Instagram (sadly one of the most used
  platform in Taiwan).
- Try to be candid and honest to myself when possible.

-- ltlnx
2023-12-19

標籤：隨想
