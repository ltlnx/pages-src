# Mobile App Suggestions

Occasionally (usually?) I encounter pain points in life, where I hope there's a mobile app that could help ease. They are documented here.

---

### "Remind Me"

Inspired by the Reddit function "!remindme" that reminds users to check back on the thread. It would be lovely if there is an app that takes anything from the "Share" dialog, especially text items, and then pop up a dialog to select an interval or fixed time to remind. Then the app adds a calendar item, and sets reminders based on the query. That way, users can be reminded through the official calendar API, which handles the push notifications.

So a typical routine would look like this:

1. Select text or other stuff and invoke the "Share" dialog
2. Select the app
3. Pop up a dialog with the following

    ```
    Title: ____________

    Reminder: ______[v] [+]

    Details: 
    +---------------------+
    | <selected text>     |
    |                     |
    +---------------------+

    Attachments: 
    <if the passed through 
    item is not text, put 
    it here>
              [Cancel] [OK]
    ```

4. Add a calendar item with "Title" as the title, "Details" as the description. Calendar items does not support attachments, so maybe think of a way to refer to them. (Or shamelessly remove the functionality)

Result: The user has a convenient way to remind themselves of things.

(Bonus: Add a widget that invokes the dialog on the homescreen?)


_Last updated: 2022-07-21_


標籤：隨想
