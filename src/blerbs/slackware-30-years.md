# Happy Birthday Slackware!

So apparently Slackware turned 30 years old on July 16, 2023. For my Slackware journey look no further than my [Slackware Impressions](https://ltlnx.tw/blerbs/slackware-impressions.html) post. Just a quick thank-you to Pat and the Slackware crew for such a stable "leading edge" system (`-current`).

Last updated: 2023-07-18

標籤：隨想
