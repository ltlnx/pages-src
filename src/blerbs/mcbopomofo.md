# 小麥注音

最近在 Mastodon 上，qbane 發了把小麥注音移植到 ibus 上的進度（[HackMD 筆記連結](https://hackmd.io/@q/ibus-mcbopomofo-dev)），才發現原來小麥注音早在幾年前就有 [fcitx 版](https://github.com/openvanilla/fcitx5-mcbopomofo)，因此我趕緊寫了個 SlackBuild 裝來試用。用一用發現，小麥注音的選字比新酷音直覺好多，在打長文的時候幾乎不用選字；打標點符號的規則也跟新注音/酷音相差不遠，很快就用上手了。

> 題外話：現在我的小麥注音 SlackBuild 已經上傳到 slackbuilds.org，各位可以點[這個連結](https://slackbuilds.org/repository/15.0/misc/fcitx5-mcbopomofo/)下載

但這麼好用的輸入法不禁讓我思考：同樣是注音自動選字輸入法，為什麼有的用起來比較順，有的用起來會卡卡的？仔細一看[小麥注音的網站](https://mcbopomofo.openvanilla.org/)，下面有個 FAQ，其中一項正是：「選字的原理是什麼？」可惜 n-gram 什麼的我實在看不懂，只能很膚淺地說它真的很好用¯\\\_(ツ)\_/¯ 看得懂的朋友們可以去他們的網站連結看看細節。

下面還有另一個有趣的細節，就是「詞條資料來源」。他們在[另一個頁面](https://mcbopomofo.openvanilla.org/textpool.html)詳述資料來源：有新聞稿、近代古典文學、各類公開出版品，與主要開發者們的部落格文字。

仔細查了一下，發現小麥注音的開發者似乎都很大咖；[相關期刊文章](https://arxiv.org/pdf/0704.3665.pdf)甚至可以看到許聞廉先生（自然輸入法開發者）當共同作者。但新酷音陣營似乎也不遑多讓，由 jserv（黃敬群老師）打頭陣；但相關學術文章就沒找到了（不代表是什麼壞事）。但 jserv 的[部落格文章](https://web.archive.org/web/20110209144124/http://blog.linux.org.tw/~jserv/archives/2010/04/_ximiiimf_hidek.html)中，也提到許多查資料時常遇到的人名，如 kanru、kcwu、gugod、zonble、pcman 等，大家好像跟各路輸入法框架（gcin、fcitx）也有些關係。這個區塊的世界似乎滿小的（笑）

2023 高文偉 @ 政大總圖

Last updated: 2023-01-03

標籤：隨想
