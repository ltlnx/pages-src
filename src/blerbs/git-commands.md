# 很常忘掉的 Git 指令

因為最近在測試 Inkscape（一兩週後要發新版本！），常用幾個指令，但因為每次用都會忘掉，決定寫一篇在這裡提醒自己。

## 刪除 Git 分支

```
git push -d <遠端名稱> <分支名稱>   # 刪除遠端分支
git branch -d <分支名稱>            # 刪除在地分支
```

如果第二個指令有錯誤，執行

```
git branch -D <分支名稱>
```

強制刪除。

來源：[How do I delete a Git branch locally and remotely?](https://stackoverflow.com/questions/2003505/how-do-i-delete-a-git-branch-locally-and-remotely)（StackOverflow）

## 簽出 Gitlab 合併請求

```
git fetch origin merge-requests/<合併請求編號>/head:<本地分支名稱>
git checkout <本地分支名稱>
```

本地分支名稱可以自己取。

來源：[How to checkout merge request locally, and create new local branch?](https://stackoverflow.com/questions/44992512/how-to-checkout-merge-request-locally-and-create-new-local-branch)（StackOverflow）

-- ltlnx
2024-10-02

<!--
COMMENTS: 113238495630137327
-->

標籤：隨想
