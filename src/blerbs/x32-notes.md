# 音控幹訓筆記 Day 1

### 音控角色

- 音控有分：**音控** 跟 **系統工程師**
    - 系統工程師負責調整音場聽起來舒服（每個地方聽起來差不多）
    - 音控負責台上聲音

### 設備

- MIC：分為電容與動圈
    - 電容 MIC **需要供電**，動圈不用
- DI：將非平衡訊號轉為平衡訊號
    - 主動式 DI：只要**需要供電**的都叫主動式
- X32：主要音控台，與 Stage Box、Input、Output 相連
- S16 / S32：Stage Box，需要軌數較多時使用，可以串接
     - 與 X32 使用 **AES50** 相連
     - AES50 有分 A/B 孔，在X32上兩個都可以接，但在Stage Box上**一律接 AES50-A**
- 喇叭：分為主動式與被動式
    - 主動式喇叭本身要供電，擴大機在喇叭內
    - 被動式喇叭沒有外接電源，但需要**擴大機**推動

### 訊號從輸入到輸出

- 輸入
    - Input：任何輸入來源都叫 Input，需要先用 Routing 對到實際的軌數上
    - Channel：輸入後的訊號對到任意軌數上，那個軌數就叫 Channel
- 訊號處理
    - Gain：調整訊號輸入大小
    - Dynamics：調整**在 Input 多少時要 Output 多少**
        - Compressor：將較大的輸入大小「壓縮」，讓輸出不會太大或差太多
        - 各軌 Compressor 壓法：
            1. 如果不需要特別 Compress，每軌都開在該輸入**可能會出現的最大音量**，以保護輸出
            2. Compressor 的 Attack **調成 0**，讓他在超過閥值時馬上作動
            3. Knee 是聲音調整的平滑度，建議開 1 或不要開太大，以免在還沒到閥值時就被壓縮
        - Compressor 的 Filter Frequency：調整**哪些頻率不需要 Compress**，如Vocal可以做 Filter Frequency 的 Low Shelf 跟 High Shelf，防止 feed 跟讓聲音比較自然（note: 確定一下是不是 Shelf）
        - Peak vs RMS Compression：Peak 代表只要有訊號超過閥值，就會被 Compress；RMS 代表聲音的平均，只有在訊號超過閥值一段時間才會作動
        - 建議先將訊號源處理好再做 Dynamics
    - EQ：調整各頻率的訊號大小
    - Insert：將訊號送到外部處理後再送回，如 Effects
        - Insert 的 pre 是 pre-EQ，post 是 post-dynamics
- 輸出
    - Bus（MixBus）：輸出到一個 Buffer，可以直接輸出也可以做其他用途
        - Bus 裡面的 Fader 大小是相對於外場聲音（0 就是跟外場相同）
        - Bus 的 Pre-Fader 是 pre 外場的 Fader
    - Main LR：主要輸出
        - 通常 Main LR 都要開到 0，而且不會掛效果、EQ等，要拿到 Matrix 再處理
    - Matrix：喇叭輸出
        - 用 Matrix 輸出的好處：可以個別調音響的 EQ 與輸出大小
        - Matrix 可以送 Main，也可以送 Bus

### 試音方法

- 一開始把 Routing 固定好後，先用 Sends on Fader 把各 Channel 送進 Bus 中
    - 如：調好貝斯後，問說「誰需要貝斯」然後一次送
- Channel 在定 Gain 時可以先把 Fader 調到 0

### Routing

- DCA：將某些軌道或 Bus 連在一起，可以一起調整音量（或大小）
    - 注意 Bus 跟 FX 都可以串 DCA

### Effects

- X32 上 Effects 會分 Send 跟 Return，Send 的音就是所謂的 dry （還沒過效果器的），Return 則是 wet （過完效果器的），用法不同
- reverb、delay 主要需要調的參數：
    - pre-delay：多久之後效果器才會作動
    - size：模擬的空間大小，太大會讓聲音不自然
    - decay：效果器的效果要延續多久

### 未分類

- 下小鼓皮不可以反相，因為小鼓構造本身會讓低頻反相
- 用低頻測相位：全放低頻（低頻的 sin wave）→ 需要調的音響開反相 → 調到差異最大 → 關反相
- 立體聲軌道送 Mono 或是 Pan 極左極右會多出 3dB
- 2 倍音量是加 6dB （大概），10 倍是加 20dB （定義上）

_Last updated: 2023-04-23_

標籤：隨想
