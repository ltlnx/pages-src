# ltlnx's Personal Task Manager

I wrote this for my convenience to manage tasks and show them on the desktop (with the [Text Viewer Widget](text-viewer-widget.html) KDE applet). It can:

- add tasks
- remove tasks
- mark tasks as done (and undo it)
- clear the task file
- return the task count
- render the task list into a Markdown document

<h3>See it in action</h3>
<video controls src="res/task-manager-demo.webm"></video>

_Problems watching the video? Try [downloading it](res/task-manager-demo.webm)._

### Interop with Bash

I modified my `.bashrc` to integrate some functionality into Bash: 

- To print the task list anytime Bash starts (e.g. opening up a terminal emulator):

  ```
  # print tasklist on start
  echo "Tasks =========================="
  tasks
  echo "================================"
  ```

- To show the task count on the Bash prompt:

  ```
  Tasks() {
      printf "[$(tasks count) tasks]"
      return 0
  }
  PS1='$(Tasks) \e[94;1m\w\e[m \$ '
  ```

  It makes the prompt look like this:

  <pre><code>[3 tasks] <span style="color:#3daee9;font-weight:bold;">~</span> $ </code></pre>

Download the script here: [tasks.sh](res/tasks.sh)

Remember to `chmod +x` the script to make it run.

_Last updated: 2023-01-14_

標籤：軟體
