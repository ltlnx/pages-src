# Basic Chinese Character Sets

(*Note: This page contains Traditional Chinese translations of English
text.*)

As Chinese speakers and learners know, Chinese has a *humongous* number
of characters, but only a subset of them are used frequently. Therefore
various organizations or font foundries try to define a plane of
characters fonts should choose to support depending on the use-case.

The following list shows some of these planes, and other ones I
collected from sources.

------------------------------------------------------------------------

## JustFont 7000

> Author: [justfont](https://justfont.com/)  
> Source: [Introductory article by
> justfont](https://justfont.com/jf7000)

Justfont (styled as "justfont") is a Taiwan font foundry and typography
education group. This is a basic 7000-character plane they released,
along with "plugin packs" to cater different needs.

File link:

- Text only: [jf7000-0.9.zip](/res/jf7000-0.9.zip)

- Spreadsheet with Unicode points and explanations for some glyphs:
  [jf7000-0.9.ods](/res/jf7000-0.9.ods)

- Original:
  [Text](https://drive.google.com/drive/u/2/folders/1LO_IRwZXb-dsti7Tb-kbT5xZijsHeUC8)
  [Spreadsheet](https://docs.google.com/spreadsheets/d/1yI9O2_nlEjP28McIXPJS8GO2U9yVBbnZIvlDFyCKV18/edit)

- Recieved: 2023-11-30

## Moedict Characters with Definitions

> Author: Me (ltlnx)  
> Source: Excerpted from [Moedict](https://moedict.tw)
> (*[Github](https://github.com/audreyt/moedict-webkit)*)  
> Character count: 9909

Moedict is a FOSS online dictionary, with sources derived from the
[Revised Mandarin Chinese
Dictionary](https://dict.revised.moe.edu.tw/search.jsp) (*zh-TW*). These
characters are extracted from Moedict's source JSON files.

File link: [moedict-chars.txt](/res/moedict-chars.txt) (*TXT, 35.3KiB*)

## Additional Moedict Characters

> Author: Me (ltlnx)  
> Source: Excerpted from [Moedict](https://moedict.tw)
> (*[Github](https://github.com/audreyt/moedict-webkit)*)  
> Character count: 550

File link:
[moedict-additional-chars.txt](/res/moedict-chars-additional.txt) (*TXT,
1.7KiB*)

## Common Mandarin Chinese Characters

> Author: Ministry of Education, ROC (Taiwan)  
> Source: [Original
> PDF](https://language.moe.gov.tw/001/Upload/Files/site_content/download/mandr/%E6%95%99%E8%82%B2%E9%83%A84808%E5%80%8B%E5%B8%B8%E7%94%A8%E5%AD%97.pdf),
> [Adapted text file by
> ButTaiwan](https://github.com/ButTaiwan/cjktables/blob/master/taiwan/edu_standard_1.txt)  
> Character count: 4808

The common character set and glyph regulations released by the Ministry
of Education, ROC. Published in June 1979. Contains 4808 characters.

File link: [edu_standard_1.txt](/res/edu_standard_1.txt)

- [Original](https://github.com/ButTaiwan/cjktables/blob/master/taiwan/edu_standard_1.txt)
- Recieved: 2022-07-14

## Secondary Mandarin Chinese Characters

> Author: Ministry of Education, ROC (Taiwan)  
> Source: [Original index
> site](https://language.moe.gov.tw/001/Upload/files/SITE_CONTENT/M0001/MU/mub.htm),
> [Adapted text file by
> ButTaiwan](https://github.com/ButTaiwan/cjktables/blob/master/taiwan/edu_standard_2.txt)  
> Character count: 6343

The secondary character set released by the Ministry of Education, ROC.
Published in 1982, revised in 1993. This revision contains 6343
characters.

File link: [edu_standard_2.txt](/res/edu_standard_2.txt)

- [Original](https://github.com/ButTaiwan/cjktables/blob/master/taiwan/edu_standard_2.txt)
- Recieved: 2022-07-14

_Last updated: 2023-11-30_

標籤：字體排印
