# Videos Where I Play Drums

I mainly play the drums. These are links to videos where I play drums.

_Last updated: 2022-12-14_

---

### NCCU Comp Sci Night, April 2022

**伍佰 & China Blue - 被動**

[Youtube link](https://www.youtube.com/watch?v=cxJMplc4kgo) | [Piped link](https://piped.kavin.rocks/watch?v=cxJMplc4kgo) (_piped.kavin.rocks_) | [Invidious link](https://invidious.snopyta.org/watch?v=cxJMplc4kgo) (_invidious.snopyta.org_)

**伍佰 & China Blue - 愛你一萬年**

[Youtube link](https://www.youtube.com/watch?v=J81-P1jaEqg) | [Piped link](https://piped.kavin.rocks/watch?v=J81-P1jaEqg) (_piped.kavin.rocks_) | [Invidious link](https://invidious.snopyta.org/watch?v=J81-P1jaEqg) (_invidious.snopyta.org_)

**草東沒有派對 - 鬼**

[Youtube link](https://www.youtube.com/watch?v=ohiEhfOC3cg) | [Piped link](https://piped.kavin.rocks/watch?v=ohiEhfOC3cg) (_piped.kavin.rocks_) | [Invidious link](https://invidious.snopyta.org/watch?v=ohiEhfOC3cg) (_invidious.snopyta.org_)

**草東沒有派對 - 山海**

[Youtube link](https://www.youtube.com/watch?v=tBDgwrmpAGk) | [Piped link](https://piped.kavin.rocks/watch?v=tBDgwrmpAGk) (_piped.kavin.rocks_) | [Invidious link](https://invidious.snopyta.org/watch?v=tBDgwrmpAGk) (_invidious.snopyta.org_)

**前方 / Help Me**

Two home-grown songs by friends and me.

[Youtube link](https://www.youtube.com/watch?v=N9MlnMmMhf8) | [Piped link](https://piped.kavin.rocks/watch?v=N9MlnMmMhf8) (_piped.kavin.rocks_) | [Invidious link](https://invidious.snopyta.org/watch?v=N9MlnMmMhf8) (_invidious.snopyta.org_)

**YOASOBI - 群青**

[Youtube link](https://www.youtube.com/watch?v=Ozlsiffonnk) | [Piped link](https://piped.kavin.rocks/watch?v=Ozlsiffonnk) (_piped.kavin.rocks_) | [Invidious link](https://invidious.snopyta.org/watch?v=Ozlsiffonnk) (_invidious.snopyta.org_)

### NCCU rock club event, March 2022

**LITE - Ghost Dance** 

[Youtube link](https://www.youtube.com/watch?v=JkbxCTSMeYw) | [Piped link](https://piped.kavin.rocks/watch?v=JkbxCTSMeYw) (_piped.kavin.rocks_) | [Invidious link](https://invidious.snopyta.org/watch?v=JkbxCTSMeYw) (_invidious.snopyta.org_)

**柯智棠 - 無關拯救的事**

[Youtube link](https://www.youtube.com/watch?v=7YaKzsjmuD4) | [Piped link](https://piped.kavin.rocks/watch?v=7YaKzsjmuD4) (_piped.kavin.rocks_) | [Invidious link](https://invidious.snopyta.org/watch?v=7YaKzsjmuD4) (_invidious.snopyta.org_)

**TRASH - 重感情的廢物**

[Youtube link](https://www.youtube.com/watch?v=nABNfe41ViI) | [Piped link](https://piped.kavin.rocks/watch?v=nABNfe41ViI) (_piped.kavin.rocks_) | [Invidious link](https://invidious.snopyta.org/watch?v=nABNfe41ViI) (_invidious.snopyta.org_)

### Christmas 2021

**Muse - Hysteria**

[Youtube link](https://www.youtube.com/watch?v=ZvTg4bu4jMs) | [Piped link](https://piped.kavin.rocks/watch?v=ZvTg4bu4jMs) (_piped.kavin.rocks_) | [Invidious link](https://invidious.snopyta.org/watch?v=ZvTg4bu4jMs) (_invidious.snopyta.org_)


標籤：音樂
