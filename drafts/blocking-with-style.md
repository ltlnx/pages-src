# 使用 uBlock Origin 與 CSS 阻擋網站部分內容

都 2024 了，你還在乖乖地全盤接受網站塞給你的東西嗎？是時候反抗了！

## 用途

最近辦了 Pinterest，但在瀏覽首頁時，常會看到像 Instagram 連續短片形式的影片夾雜出現。如果不想看到它們，最直覺的方法當然是直接用 class 來阻擋。問題是，現代網頁程式可能在重新整理網頁之後 class 就變了樣，也就意味著我們不能拿它來當阻擋的目標。那我們要怎麼阻擋呢？

很簡單，用兩種網頁一定會有、高機率不會變的東西：**文字**與**無障礙標籤**。

## 觀察、決定阻擋的元素

首先，我們需要觀察能用來阻擋的目標。以 Pinterest 的首頁舉例，我們能看到影片都會有時間戳記*（圖一）*。我們在時間戳記上按右鍵，發現無法直接選取時間戳記，因為游標在上方懸浮時，會轉到另一個畫面*（圖二）*。於是我們只好直接按右鍵開啟檢測器（右鍵 → 「檢測...」），並在上方的「搜尋 HTML」輸入框中輸入時間戳記的文字，試圖定位該時間戳記*（圖三）*。定位成功之後，我們繼續觀察這個標籤有沒有能用來阻擋的元素。標籤中名為「data-test-id」的屬性取得了我的注意，於是我在上方的搜尋框繼續搜尋其他的時間戳記是否有此屬性*（圖四）*。

這時候我們能確定，Pinterest 網站在標籤上使用 `data-test-id="PinTypeIdentifier"` 時，外層很可能是影片。確定這點之後，我們就可以來寫阻擋規則了。

<details><summary>圖片（點擊以開啟/關閉）</summary>
<table><tr><td>
![【圖一】時間戳記標示](res/pinterest-timestamp.png)
</td><td>
![【圖二】游標懸浮時顯示的是「儲存」按鈕及另外兩個圖示](res/pinterest-hover.png)
</td></tr><tr><td>
![【圖三】定位成功，搜尋到含時間的 div](res/pinterest-finddiv.png)
</td><td>
![【圖四】可以看到，我們搜尋到了另一個時間戳記使用相同的「PinTypeIdentifier」字串](res/pinterest-findmore.png)
</td></tr></table>
</details>

## 撰寫、試用規則

找到目標後，再來要撰寫適用的規則來阻擋它們。首先我們打開 uBlock Origin 對話窗，並點擊「滴管」按鈕啟動元素過濾模式。再來選擇隨便一個元素，以啟用元素過濾對話窗。

元素過濾對話窗顯示之後，我們就可以開始撰寫規則了。我主要會使用的有三種元素選取方法：

<style>
html {max-width: initial}
p,#header,#footer,h1,h2,h3,h4,ul,ol,summary {max-width: 670px; margin-left: auto; margin-right: auto}
table {max-width: 900px; margin: auto;}
figure {margin: 0}
td {background: #e5dedb; padding: 16px; text-align: center}
img {max-height: 320px; width: initial; max-width: initial}
figcaption {height: 2.5rem}
@media (max-width: 900px) {
  td {display: block; background: none}
  img {width: 100%; height: unset; max-height: unset;}
}
</style>
