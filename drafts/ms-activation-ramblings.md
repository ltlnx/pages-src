## Ramblings

I value your sanity so only if you have spare time and you can put up with my story-telling skills, read further. I've warned you.

<details>
<summary>Proceed with caution</summary>

## Windows Versions

Since I'm installing Windows for school work, of course I wanted to install a version compatible with the most software I'll be running, so I chose Windows 10. Not the regular version of Windows 10, though —— there's a special version Microsoft mainly deploys on enterprise systems, kiosks and POS systems, called **Windows 10 LTSC**, or Long Term Service Channel. It's your regular Windows 10 but without the bloat.

The download link isn't hard to find, but it's somehow harder to find language-specific versions, in my case for Traditional Chinese. During my search I found a [file on Github](https://github.com/massgravel/mas-docs/blob/main/windows_10_links.md) whose repository leads to a [website of a tool](https://massgrave.dev/index.html) made to download and activate Windows and Office products. 

The website is really lovely. The homepage explains what the tool is and how to run it. Got no Windows or Office? The "Download Windows/Office" button on the top links to a page solely for downloading an ISO or installer for Windows and Office. The website has a great structure, with a clear interface and zero nonsense.

I downloaded Windows 10 LTSC 2021 and Office 2016. After that, the next step is to prepare a virtual machine.

## The Virtual Machine, first pass

For the virtual machine front-end I chose virt-manager, for no particular reason other than "I installed it and I didn't want to remove it and install another method". Virtualbox seems to support Windows better, so you can use that. However virt-manager is workable too, so if you have it, don't worry.

The interface of virt-manager is intuitive enough to create a virtual machine. The only snag is the file picker: the quickest way to select ISOs is to open the dialog, click "Explore locally" button at the bottom right, and select from the file picker _there_. Why don't they make it into one dialog? I don't get it.

Another thing I encountered is the virtual machine is unable to start because it can't find the backend for USB redirecting. If you face a similar problem, the solution is to click "Open Settings before installing" checkbox, click "Finish", remove the offending devices by clicking on the _bottom-right_ "Remove" button, then clicking the _top-left_ "Continue Installation" button. Really great UI, huh.

The installation started, and apart from Windows shenanigans like having to disable all telemetry by hand and having to answer three security questions, the installation went pretty straightforward. However I know nothing about the gross parts that are coming later on.

</details>
