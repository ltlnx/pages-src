# 萬用的文件轉換工具 Pandoc

Pandoc 是在日常生活中不常用到，但必須備著的工具，用來轉換各種格式的文件。有點像文字版的格式工廠（時代的眼淚）與 ffmpeg。

## 日常生活的用途

在抄筆記、寫作業時，往往老師是給 Word 檔，但對於習慣用 Vim 的我來說，開啟、編輯 Word 檔有時會覺得綁手綁腳。這時候只要在包含檔案的資料夾中執行一個命令，我們假設檔案名稱為 `apuntes.docx`：

```
pandoc -f docx -t markdown apuntes.docx -o apuntes.md
```

當然，在編輯完之後也可以轉換回 Word 格式：

```
pandoc -f markdown -t docx apuntes.md -o apuntes-new.docx
```

另一個常用的地方是將 Markdown 格式文字轉換為 HTML，讓結果可以在網路上顯示：

```
pandoc -f markdown -t html apuntes.md -o apuntes.html
```

當然，轉換出的 HTML 檔案是尚未加入 CSS 美化的。除了自己寫 CSS 美化之外，Pandoc 也提供了公版：

```
pandoc -s -f markdown -t html apuntes.md -o apuntes.html
```

這些就是我最常用的指令，在 Pandoc 網站上有更多[檔案格式與選項的例子](https://pandoc.org/demos.html)。

當然，我的整個部落格也是用 Pandoc 產生的，有來源資料夾與目標資料夾，由 Pandoc 將來源資料夾的 Markdown 檔案轉換並放入目標資料夾。

## 所以 Pandoc 是什麼？

Pandoc 是可以在


從這裡應該可以看出 Pandoc 的命令格式：

![Pandoc 命令格式](/res/pandoc.svg)
