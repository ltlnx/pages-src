#!/bin/bash
# ltlnx's blog updater script, rewritten 20231103

# TODO
# - some sort of help on the command line
# - implement incremental updates
# - augument sample description and markdown files

# so... multilingual support? It's the best I can do though
vgenlang="zh_TW"
if echo $vgenlang | grep -q "zh_TW"; then
    vtagstext="標籤"
    vcolon="："
    varchivetext="庫存"
    vnodesctext="無簡介。"
else
    vtagstext="Tags"
    vcolon=": "
    varchivetext="Archive"
    vnodesctext="No description."
fi


# messages and errors
msg() {
    printf "[MSG] $@\n"
}
die() {
    printf "[ERR] $@\n" && exit 1
}

# config stuff and initialization
vconfig="$(dirname "$0")/.updaterc"
source "$vconfig"

# test if things are there and if not, bail out
E=0
if [ ! -d "$vroot/$vsrcname" ]; then
    msg "Source folder $vroot/$vsrcname not found." && E=1
fi
if [ ! -d "$vroot/$vdstname" ]; then
    msg "Destination folder $vroot/$vdstname not found." && E=1
fi
if [ ! -f "$vroot/$vheadername" ]; then
    msg "Header file $vroot/$vheadername not found." && E=1
fi
if [ ! -f "$vroot/$vfootername" ]; then
    msg "Footer file $vroot/$vfootername not found." && E=1
fi
if [ -z "$vmdconvcommand" ]; then
    msg "Markdown conversion command not set; defaulting to pandoc."
    vmdconvcommand="pandoc -f markdown -t html"
fi
if [ -z "$vname" ]; then
    msg "Name not set; defaulting to the blog title."
    msg "(Sadly Atom feeds require an author to be valid)"
    vname="$vtitle"
fi
if [ "$E" -ne 0 ]; then
    die "Please fix them in the config $vconfig, or run \`$0 init\` to\ncreate a new set of files."
fi

# sitemap generation
gensitemap() {
    cd "$vroot/$vdstname" || die "Either you haven't created the destination folder, or the config has wrong settings. Please check if everything is set up."
    # initial overhead
    cat > "sitemap.xml" <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
EOF
    # items
    for item in $(find . -name "*.html" -printf "%P\n"); do
        # get info of markdown file
        it_loc="${vurl}/$item"
        it_lastmod="$(date -d "$(cat "$item" | grep -Po '^<p>.*[\_]*Last [[:alpha:]]*: \K[^\<]*' || cat "$item" | grep -o "^[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}$" || date +"%Y-%m-%d")" -R)"
        cat >> "sitemap.xml" <<EOF
<url>
 <loc>$it_loc</loc>
 <lastmod>$it_lastmod</lastmod>
</url>
EOF
    done
    # ending
    cat >> "sitemap.xml" <<EOF
</urlset>
EOF
    cd -
}

# quick actions
case "$1" in
    deploy)
        curdir="$(pwd)"
        test "$2" || die "Please input a commit message"
        test "$3" && die "Please put the commit message in quotes"
        gensitemap
        cd "$vroot"/"$vdstname" || die "Either you haven't created the destination folder, or the config has wrong settings. Please check if everything is set up."
        git add . || die "The destination directory is not versioned by git."
        git commit -m "$2"
        git push
        cd "$curdir"
        exit 0
        ;;
    gensitemap)
        gensitemap
        exit 0
        ;;
    serve)
        cd "$vroot"/"$vdstname" || exit 1
        python3 -m http.server || die "Your system probably does not have Python 3 installed. Please change the line invoking python3 into your own web server command, or install Python 3."
        cd -
        exit 0
        ;;
esac

# copy the whole source dir to destination
rm -r $vroot/$vdstname/*
cp -r $vroot/$vsrcname/* "$vroot/$vdstname"

# kickstart the RSS feed
cat > "$vroot/$vdstname/$vfeedname" <<EOF
<?xml version="1.0" encoding="utf-8" ?>
<feed xmlns="http://www.w3.org/2005/Atom" xml:lang="$vlang">
  <title>$vtitle</title>
  <id>$vurl/</id>
  <updated>$(date +%Y-%m-%dT%T%:z)</updated>
  <author>
    <name>$vname</name>
  </author>
  <subtitle>$vdesc</subtitle>
  <rights>$vcopyright</rights>
  <link href="$vfeedurl" rel="self"/>
EOF

# post conversion sequence definition
vsrclist="$(find "$vroot/$vsrcname" -path "*.md" -printf "%p\n" | while read -r file; do
    date="$(cat "$file" | grep -o "^[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}")" \
        || date="$(cat "$file" | grep -Po '^[_]*Last [^:]*: \K[^\_]*')"
    printf "$vsrclist$date $file\n"
done)"

# post conversion
# the code is dense, yeah, I'm sorry.
mkdir -p "$vroot/$vdstname/tags"
printf "$vsrclist" | sort -rn | cut -d ' ' -f 2 | while read -r file; do
    msg "Processing $file"
    filebn="$(basename "$file" .md)"
    # the title should be on the first three lines (preferably the first)
    # and starts with a markdown # (single hash sign)
    title="$(head -n 3 "$file" | grep -Po "^# \K.*$" | sed "s|<[^>]*>||g")"
    # for the date we support 2 schemes:
    # one is the date itself on one line, another is "Last <any word>: <date>",
    # with or without italic `_` marks around them.
    date="$(cat "$file" | grep -o "^[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}")" \
        || date="$(cat "$file" | grep -Po '^[_]*Last [^:]*: \K[^\_]*')"
    # there may be unfortunate cases where multiple dates are found,
    # for which we pick the first one
    date="$(echo "$date" | head -n 1)"
    # year for categorizing
    year="$(echo "$date" | grep -o "[0-9]\{4\}")"
    # tags start with "Tags: " of "tags: ", and is comma-separated
    tags="$(cat "$file" | grep -Po "^$vtagstext$vcolon\K.*" | grep -Po '(?!( ))[^,]*' | sed 's| |-|g')" 
    test "$tags" && htmltaglist="<span class=\"tags\">$vtagstext$vcolon$(printf "$tags\n" | while read -r line; do printf "<a href=\"/tags/$line.html\">$line</a> "; done)</span>"
    # just define the link as the filename, no more categorizing by year
    link="/$filebn.html"
    # duplicate filename solving
    if [ -f "$vroot/${vdstname}${link}" ]; then
        link="$(echo "$link" | sed "s|.html|-$(find "$vroot/$vdstname" -name "$filebn-[0-9]*.html" | wc -l).html|")"
    fi
    # get comments ID from file
    commentsid="$(cat "$file" | grep -Po "^COMMENTS: \K[0-9]*")"
    if [ -n "$commentsid" ]; then
        comments="$(cat $vroot/comments | sed "s|{{COMMENTSID}}|$commentsid|")"
    else
        comments=""
    fi
    # all titles are generated as
    # <the h1 title> - <blog name>
    cat > "$vroot/${vdstname}${link}" <<EOF
$(cat "$vroot/$vheadername" | sed "s|{{TITLE}}|$title - $vtitle|")
$(cat "$file" | sed "s|^$vtagstext$vcolon.*$|$htmltaglist|g" | $vmdconvcommand)
$(cat "$vroot/$vfootername" | perl -pe "s|{{LINK}}|$link|;s|{{COMMENTS}}|$comments|")
EOF
    postcontent="$(head -n 30 "$file" | grep -v "> " | sed "s|<[^>]*>||g" | sed -n '/^# /,/^.*/d; 1,/^$/p' | grep . )"
    entry="::: postblock
## [$title]($link)
<span class=\"date\">$date</span>$htmltaglist

$postcontent

:::"
    # throw the entry into the appropriate catalogs
    # throw it into the archive if it's not a sticky post
    if ! (echo "$file" | grep -q "$vsrcname/sticky"); then
        echo "$entry" >> "$vroot/$vdstname/archive.md"
        # generate rss entry
        if [ -n "$date" ]; then
            pubDate="$(date -d "$date" +%Y-%m-%dT%T%:z)"
        else
            pubDate="2020-01-01T00:00:00+08:00"
        fi
        cat >> "$vroot/$vdstname/$vfeedname" <<EOF
  <entry>
    <title>$title</title>
    <link rel="alternate" href="$vurl$link"/>
    <id>$vurl$link/</id>
    <author>
      <name>$vname</name>
    </author>
    <updated>$pubDate</updated>
    <summary>$postcontent</summary>
  </entry>
EOF
    fi
    if [ -n "$tags" ]; then
        printf "$tags\n" | while read -r tag; do
            echo "$entry" >> "$vroot/$vdstname/tags/$tag.md"
        done
    fi
done

# make tag indexes into proper pages
find "$vroot/$vdstname/tags" -path "*.md" | while read -r file; do
    filebn="$(basename "$file" .md)"
    title="$(echo "$filebn" | sed 's|-| |g;s|\b\(.\)|\u\1|g')"
    link="/tags/$filebn.html"
    desc="$(cat "$vroot/$vsrcname/$filebn.desc")" || desc="$vnodesctext\n"
    content="$(cat "$file")"
    cat > "$vroot/${vdstname}${link}" <<EOF
$(cat "$vroot/$vheadername" | sed "s|{{TITLE}}|$title - $vtitle|")
$(printf "$desc\n\n::: postcardblock\n\n$content\n\n:::\n" | $vmdconvcommand)
$(cat "$vroot/$vfootername" | sed "s|{{LINK}}|$link|")
EOF
    echo " [$title]($link)" >> "$vroot/$vdstname/tags/index.md"
done

# make tag page
desc="$(cat "$vroot/$vsrcname/tags.desc")" || desc="$vnodesctext\n"
tagscontent="$(cat "$vroot/$vdstname/tags/index.md")"
cat > "$vroot/$vdstname/tags/index.html" <<EOF
$(cat "$vroot/$vheadername" | sed "s|{{TITLE}}|$vtagstext - $vtitle|")
$(printf "$desc\n\n::: postcardblock\n\n$(printf "$tagscontent" | sed 's/^/-/g')\n\n:::\n" | $vmdconvcommand)
$(cat "$vroot/$vfootername" | sed "s|{{LINK}}|/tags/index.html|")
EOF

# make archive page
desc="$(cat "$vroot/$vsrcname/archive.desc")" || desc="$vnodesctext\n"
content="$(cat "$vroot/$vdstname/archive.md")"
cat > "$vroot/$vdstname/archive.html" <<EOF
$(cat "$vroot/$vheadername" | sed "s|{{TITLE}}|$varchivetext - $vtitle|")
$(printf "$desc\n\n$vtagstext$vcolon$tagscontent\n\n::: postcardblock\n\n$content\n\n:::\n" | $vmdconvcommand)
$(cat "$vroot/$vfootername" | sed "s|{{LINK}}|/archive.html|")
EOF

# close RSS generation
echo "</feed>" >> "$vroot/$vdstname/$vfeedname"

# move archived pages into destination
cp $(find "$vroot/$vsrcname/archive" -path "*.html" -printf "%p ") "$vroot/$vdstname" 

# remove leftover files and empty directories in the destination
find "$vroot/$vdstname" -path "*.md" | xargs rm || true
find "$vroot/$vdstname" -path "*.desc" | xargs rm || true
find "$vroot/$vdstname" -mindepth 1 -type d -empty -delete || true
